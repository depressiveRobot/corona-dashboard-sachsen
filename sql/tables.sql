CREATE TABLE kreise_sachsen (
    id integer PRIMARY KEY,
    ags integer ,
    name character varying(50),
    geom geometry(MultiPolygon),
    "population (31.12.18)" integer,
    gesundheitsamt_url text,
    "order" integer
);

CREATE TABLE measurement (
    id SERIAL PRIMARY KEY,
    kreis_id SERIAL REFERENCES kreise_sachsen(id),
    date timestamp without time zone,
    source text,
    value double precision,
    type text,
    delta_yesterday double precision,
    previous_measurement_id integer REFERENCES measurement (id),
    created timestamp without time zone DEFAULT now(),
    unique (kreis_id, date, type, source)
);

CREATE TABLE links (
    id SERIAL PRIMARY KEY,
    kreis_id bigint REFERENCES kreise_sachsen(id),
    text text,
    link text,
    created timestamp without time zone DEFAULT now(),
    source text
);