<p xmlns:dct="http://purl.org/dc/terms/" xmlns:cc="http://creativecommons.org/ns#" class="license-text"><a rel="cc:attributionURL" href="https://gitlab.com/gerbsen/corona-dashboard-sachsen"><span rel="dct:title">Corona Dashboard</span></a> by <a rel="cc:attributionURL" href="danielgerber.eu"><span rel="cc:attributionName">Daniel Gerber</span></a>CC BY-NC-SA 4.0<a href="https://creativecommons.org/licenses/by-nc-sa/4.0"><img style="height:22px!important;margin-left: 3px;vertical-align:text-bottom;" src="https://search.creativecommons.org/static/img/cc_icon.svg" /><img  style="height:22px!important;margin-left: 3px;vertical-align:text-bottom;" src="https://search.creativecommons.org/static/img/cc-by_icon.svg" /><img  style="height:22px!important;margin-left: 3px;vertical-align:text-bottom;" src="https://search.creativecommons.org/static/img/cc-nc_icon.svg" /><img  style="height:22px!important;margin-left: 3px;vertical-align:text-bottom;" src="https://search.creativecommons.org/static/img/cc-sa_icon.svg" /></a></p>

# Corona Dashboard Sachsen

Visualisierungen zur Corona Pandemie in Sachsen

## Installation und Starten
Um alle Daten komplett zu importieren, folgende Befehle ausführen.

```
CREATE USER $USER;
CREATE DATABASE $DATABASE_NAME;
GRANT ALL PRIVILEGES ON DATABASE $DATABASE_NAME TO $USER;
CREATE EXTENSION IF NOT EXISTS postgis;
```

Run a single file:
`env PGUSER=$USER PGPASSWORD=$PASSWORD ./scripts/run-import-file.sh ../../data/sms/coronavirus_04_07_2020.html`

Run via website
`env PGUSER=$USER PGPASSWORD=$PASSWORD ./scripts/run-import-single.sh`

```
npm install
env PGUSER=$USER PGPASSWORD=$PASSWORD ./script/run-all.sh
```

Run single website with docker:
```
docker build -t gerbsen/corona-dashboard .
docker run -v ${PWD}/dist:/dashboard/dist --rm -it gerbsen/corona-dashboard env PGHOST=host.docker.internal PGUSER=$PGUSER PGPASSWORD=$PGPASSWORD ./scripts/run-import-single.sh
 ```

## SQL Queries

### Group infections for saxony by week
```sql
SELECT date_part('year', date::date) as year,
       date_part('week', date::date) AS weekly,
       SUM(value)           
FROM measurement
WHERE kreis_id = 14 and type = 'infected'
GROUP BY year, weekly
ORDER BY year, weekly;
```