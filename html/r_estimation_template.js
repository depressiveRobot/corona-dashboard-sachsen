{
  type: 'lineWithErrorBars',
  data: {
    labels: R_ESTIMATION_LABELS,
    datasets: [{
      label: 'Nettoreproduktionszahl (R)',
      backgroundColor: window.chartColors.green,
      borderColor: window.chartColors.green,
      data: R_ESTIMATION_VALUES,
      fill: false,
      order: 1
    },{
      label: 'R = 1',
      data: Array.apply(null, new Array(R_ESTIMATION_LABELS_SIZE)).map(Number.prototype.valueOf, 1),
      fill: false,
      radius: 0,
      backgroundColor: window.chartColors.red,
      borderColor: window.chartColors.red
    }]        
  },
  options: {
    legend: {
      position: 'bottom'
    },
    responsive: true,
    title: {
      display: true,
      position: 'top',
      text: 'Verlauf der Reproduktionsrate R (Schätzung, 21 Tage)'
    },
    tooltips: {
      mode: 'index',
      intersect: false,
    },
    hover: {
      mode: 'nearest',
      intersect: true
    },
    scales: {
      xAxes: [{
        display: true,
        scaleLabel: {
          display: false,
          labelString: 'Datum'
        }
      }],
      yAxes: [{
        display: true,
        scaleLabel: {
          display: true,
          labelString: 'Wert von R'
        }
      }]
    }
  }
}