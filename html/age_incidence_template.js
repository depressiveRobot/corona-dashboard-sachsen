var CONFIG_NAME = {
  type: 'line',
  data: {
    labels: [AGE_INCIDENCE_DATES],
    datasets: [{
      label: '0-4 Jahre',
      backgroundColor: window.chartColors.red,
      borderColor: window.chartColors.red,
      data: [AGE_INCIDENCE_0_4_VALUES],
      fill: false,
    }, {
      label: '5-14 Jahre',
      fill: false,
      backgroundColor: window.chartColors.blue,
      borderColor: window.chartColors.blue,
      data: [AGE_INCIDENCE_5_14_VALUES],
    }, {
      label: '15-34 Jahre',
      fill: false,
      backgroundColor: window.chartColors.green,
      borderColor: window.chartColors.green,
      data: [AGE_INCIDENCE_15_34_VALUES],
    }, {
      label: '35-59 Jahre',
      fill: false,
      backgroundColor: window.chartColors.grey,
      borderColor: window.chartColors.grey,
      data: [AGE_INCIDENCE_35_59_VALUES],
    }, {
      label: '60-79 Jahre',
      fill: false,
      backgroundColor: "#000000",
      borderColor: "#000000",
      data: [AGE_INCIDENCE_60_79_VALUES],
    }, {
      label: '80+ Jahre',
      fill: false,
      backgroundColor: "#6D98BA",
      borderColor: "#6D98BA",
      data: [AGE_INCIDENCE_80_VALUES],
    }]
  },
  options: {
    legend: {
      position: "bottom",
    },
    responsive: true,
    title: {
      display: true,
      text: 'GRAPH_TITLE'
    },
    tooltips: {
      mode: 'index',
      intersect: false,
    },
    hover: {
      mode: 'nearest',
      intersect: true
    },
    elements: {
      point:{
        radius: 0
      }
    },
    scales: {
      xAxes: [{
        display: true,
        scaleLabel: {
          display: true,
          labelString: 'X_AXIS_LABEL'
        }
      }],
      yAxes: [{
        display: true,
        scaleLabel: {
          display: true,
          labelString: 'Y_AXIS_LABEL'
        }
      }]
    }
  }
};