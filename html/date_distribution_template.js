{
  type: 'bar',
  data: {
    labels: [DATE_DISTRIBUTION_LABELS],
    datasets: [{
      label: 'DATA_LABEL_1',
      backgroundColor: window.chartColors.green,
      borderColor: window.chartColors.green,
      data: [DATA_1],
      fill: false,
      hidden: false
    }]
  },
  options: {
    responsive: true,
    title: { display: true, text: 'DATE_DISTRIBUTION_LABEL' },
    tooltips: { mode: 'index', intersect: false },
    hover: { mode: 'nearest', intersect: true },
    legend: { position: 'bottom', display : false },
    scales: {
      xAxes: [{
        display: true,
        stacked: true,
        scaleLabel: {
          display: true,
          labelString: 'Datum'
        }
      }],
      yAxes: [{
        display: true,
        stacked: true,
        position: 'right',
        scaleLabel: {
          display: true,
          labelString: 'Anzahl der Infektionen'
        },
        gridLines: {
          display: true,
          drawBorder: true,
          drawOnChartArea: true,
          drawTicks: true,
        }
      }]
    }
  }
}