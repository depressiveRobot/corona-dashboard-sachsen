{
  type: 'bar',
  data: {
    labels: ['0 - 4 Jahre', '5 - 14 Jahre', '15 - 34 Jahre', '35 - 59 Jahre', '60 - 79 Jahre', '80+ Jahre'],
    datasets: [{
      label: 'Männlich',
      backgroundColor: window.chartColors.green,
      borderColor: window.chartColors.green,
      borderWidth: 1,
      data: [ AGE_GROUP_GENDER_MALE ]
    }, {
      label: 'Weiblich',
      backgroundColor: window.chartColors.yellow,
      borderColor: window.chartColors.yellow,
      borderWidth: 1,
      data: [ AGE_GROUP_GENDER_FEMALE ]
    }]
  },
  options: {
    responsive: true,
    legend: {
      position: 'bottom',
    },
    title: {
      display: true,
      text: 'AGE_GROUP_GENDER_LABEL'
    },
    annotation: {
      annotations: [
        IMPORTANT_DATES
      ]
    }
  }
}