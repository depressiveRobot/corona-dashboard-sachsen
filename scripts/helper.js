const commons = require('../scripts/replacements/commons.js');

const agsToKreisIdMapping = {
  "14612": 1,
  "14511": 2,
  "14713": 3,
  "14625": 4,
  "14521": 5,
  "14626": 6,
  "14729": 7,
  "14627": 8,
  "14522": 9,
  "14730": 10,
  "14628": 11,
  "14523": 12,
  "14524": 13
}

var getIncidenceTrendIcon = function(todayIncidence, yesterdayIncidence) {
  
  const changePercentage   = -(1-(todayIncidence/yesterdayIncidence))
  let icon = ""; 
  if (changePercentage > 0.2) {
    icon = "bi-arrow-up-circle" 
  }
  else if (changePercentage >= 0.05 && changePercentage <= 0.2 ){
    icon = "bi-arrow-up-right-circle" 
  }
  else if (changePercentage > -0.05 && changePercentage < 0.05 ){
    icon = "bi-arrow-right-circle" 
  }
  else if (changePercentage <= -0.05 && changePercentage >= -0.2 ){
    icon = "bi-arrow-down-right-circle" 
  }
  else icon = "bi-arrow-down-circle" 

  return `<i class="bi ${icon}"></i>`
}

var getName = function(kreis_id) {
  return commons.names[kreis_id];
};

var getColor = function(kreis_id) {
  return commons.colors[kreis_id];
};

var getLighterColor = function(kreise_id) {
  var num = parseInt(getColor(kreise_id).replace("#",""), 16),
  amt = Math.round(2.55 * 20),
  R = (num >> 16) + amt,
  B = (num >> 8 & 0x00FF) + amt,
  G = (num & 0x0000FF) + amt;
  return "#" + (0x1000000 + (R<255?R<1?0:R:255)*0x10000 + (B<255?B<1?0:B:255)*0x100 + (G<255?G<1?0:G:255)).toString(16).slice(1);
}

var getPopulationByAgs = function(ags) {
  return commons.population[commons.agsToKreisIdMapping[ags]];
};

var getIncidenceForKreisAndInfections = function(kreis_id, infections) {
  return Number((infections / commons.population[kreis_id]) * 100000).toFixed(1);
}

module.exports.getIncidenceForKreisAndInfections = getIncidenceForKreisAndInfections;
module.exports.agsToKreisIdMapping = agsToKreisIdMapping;
module.exports.getPopulationByAgs = getPopulationByAgs;
module.exports.getColor = getColor;
module.exports.getName = getName;
module.exports.getIncidenceTrendIcon = getIncidenceTrendIcon;
module.exports.getLighterColor = getLighterColor;
