var Client = require('pg-native')
var client = new Client()
client.connectSync()
const moment = require('moment');
const commons = require('../scripts/replacements/commons.js');

var get7dIncidenceForKreisToday = function(kreis_id, offset = 0, type = "7d-incidence") {
  const query = `SELECT value FROM measurement 
   WHERE source = 'rki' and kreis_id = ${kreis_id} and type = '${type}' order by date desc LIMIT 1 OFFSET ${offset};`
  return client.querySync(query)[0].value;
}

var get7dIncidenceForKreisYesterday = function(kreis_id, type = "7d-incidence") {
  return get7dIncidenceForKreisToday(kreis_id, 1, type);
}

var get7dIncidenceForKreisOneWeekBefore = function(kreis_id, type = "7d-incidence") {
  return get7dIncidenceForKreisToday(kreis_id, 7, type);
}

var getXDaysIncidenceForDistrict = function(kreis_id, days = 7, round = 2, offset = 0){
  const query = `
    SELECT ROUND((SUM(value) / (SELECT k."population (31.12.18)" FROM kreise_sachsen k WHERE id = ${kreis_id}) * 100000)::numeric, ${round}) as incidence
    FROM measurement m
    WHERE type = 'infected' 
      AND m.date::date >= CURRENT_DATE - integer '${days}' AND m.date::date < CURRENT_DATE - integer '${offset}' 
      AND m.kreis_id = ${kreis_id}
      AND m.source = '${commons.sourceForInfections}';`
  return client.querySync(query)[0].incidence;
}

var getXDaysIncidenceForDistrictOnDate = function(kreis_id, date, days = 7) {
  const query = `SELECT ROUND((SUM(value) / (SELECT k."population (31.12.18)" FROM kreise_sachsen k WHERE id = ${kreis_id}) * 100000)::numeric, 2) as incidence 
      FROM measurement m
      WHERE type = 'infected'
        AND date <= '${date}' and date > '${date}'::date - integer '${days}'
        AND m.kreis_id = ${kreis_id}
        AND m.source = 'rki'`
  return client.querySync(query)[0].incidence;
}

var getWeekValues = function(kreis_id, type, limit = 100, source = 'rki') {

  var query = `SELECT date_part('isoyear', date::date) as year,
                date_part('week', date::date) AS week,
                SUM(value)           
              FROM measurement
              WHERE kreis_id = ${kreis_id} and type = '${type}' and source = '${source}'
              GROUP BY year, week
              ORDER BY year, week DESC
              LIMIT ${limit};`
              
  return client.querySync(query).sort(function(a, b){ return b.year - a.year || b.week - a.week  });
}

var getInfectionsForDays = function(kreis_id, limit = 21, type = 'infected', source = commons.sourceForInfections){

  const query = 
    `SELECT TO_CHAR(date, 'YYYY-MM-DD') as date, value 
      FROM measurement 
      WHERE kreis_id = ${kreis_id} 
        AND source = '${source}' AND type = '${type}'
      ORDER BY date desc
      LIMIT ${limit}`;
  const result = {}
  client.querySync(query).reverse().forEach(measurement => {
    result[measurement.date] = measurement.value;
  });

  return result;
}

var getInfectionsSumForLastXDays = function(kreis_id, days){
  
  const result = {};
  const maxDate = getMaxDate();
  for (let index = days; index >= 0; index--) {
    const date = moment(maxDate).subtract(index, 'days');
    const query = 
      `SELECT SUM(value) as sum
       FROM measurement m
       WHERE kreis_id = ${kreis_id}
       AND type = 'infected' 
       AND m.source = '${commons.sourceForInfections}'
       AND date::date <= '${date.format("YYYY-MM-DD")}';`
    
    result[date.format("YYYY-MM-DD")] = client.querySync(query)[0].sum;
  }

  return result;
}

var getInfectionsForDistrictAndDate = function(kreis_id, date, type = 'infected'){

  const query = 
    `SELECT value
     FROM measurement m, kreise_sachsen k 
     WHERE kreis_id = ${kreis_id} 
     AND type = '${type}' 
     AND k.id = m.kreis_id 
     AND m.source = '${ (type == "death" || type == "death-today") ? "rki" : commons.sourceForInfections }'
     AND date::date = '${date}'`
  
  return client.querySync(query)[0].value;
}

var getTotalInfectionsForDistrict = function(measurementType, kreis_id, source){

  return client.querySync(`
    SELECT SUM(value) as sum
    FROM measurement 
    WHERE type = '${measurementType}' 
      AND kreis_id = ${kreis_id}
      AND source = '${source}';`)[0].sum;
}

var getInfectionsForDistrict = function(kreis_id){

  return client.querySync(`
    SELECT SUM(value) as sum
    FROM measurement 
    WHERE type = 'infected' 
      AND date::date >= CURRENT_DATE - integer '7' 
      AND kreis_id = ${kreis_id}
      AND source = '${commons.sourceForInfections}';`)[0].sum;
}

var getMaxDate = function(type = 'infected', source = commons.sourceForInfections){
  return client.querySync(`SELECT MAX(date::date) as date FROM measurement WHERE type = '${type}' and source = '${source}';`)[0].date;
}

var getCurrentCovidCases = function() {
  return getCurrentCovidValueFor('faelleCovidAktuell');
};

var getCurrentCovidCasesVentilated = function() {
  return getCurrentCovidValueFor('faelleCovidAktuellBeatmet');
};

var getCurrentCovidValueFor = function(type, kreis_id = 14) {

  const result = {}
  const query = `
    SELECT date::date::varchar, value
    FROM measurement m
    WHERE m.kreis_id = ${kreis_id} AND type = '${type}'
    ORDER BY date;`;
  const rows = client.querySync(query);

  rows.forEach(function(measurement){
    result[measurement.date] = measurement.value;
  });

  return result;
};

var getSingleCovidValueFor = function(type, kreis_id = 14) {

  const query = `
    SELECT date::date::varchar, value
    FROM measurement m
    WHERE m.kreis_id = ${kreis_id} AND type = '${type}'
    ORDER BY date DESC
    LIMIT 1;`;
  const res = client.querySync(query);

  return { date:  res[0].date,  
           value: res[0].value };
};


var getInfectionBetweenDates = function(kreis_id, start_date, end_date, type = 'infected'){
  const query = 
    `SELECT SUM(value) as sum
    FROM measurement m, kreise_sachsen k 
    WHERE kreis_id = ${kreis_id} 
    AND type = '${type}' 
    AND k.id = m.kreis_id 
    AND m.source = '${commons.sourceForInfections}'
    AND date::date <= '${start_date.format("YYYY-MM-DD")}' 
    AND date::date > '${end_date.format("YYYY-MM-DD")}'`
  
  return client.querySync(query)[0].sum;
}

var getTotalDeathsForDistrict = function(kreis_id){
  const query = 
    `SELECT SUM(value)
     FROM measurement m
     WHERE kreis_id = ${kreis_id} AND type = 'death' 
     AND m.source = 'rki';`
  return client.querySync(query)[0].sum;
}

var getTotalDeaths = function(){
  
  var results = {};
  commons.kreise.filter(id => id != 14).forEach(kreis_id => {
     results[getTotalDeathsForDistrict(kreis_id)] = kreis_id;
  })

  return results;
}

var getTotalInfections = function(){
  
  var results = {};
  commons.kreise.filter(id => id != 14).forEach(kreis_id => {
     results[getTotalInfectionsForDistrict('infected', kreis_id, commons.sourceForInfections)] = kreis_id;
  })

  return results;
}

var getHealthOfficeUrl = function(kreis_id){
  const query = 
    `SELECT gesundheitsamt_url 
     FROM kreise_sachsen
     WHERE id = ${kreis_id};`
  return client.querySync(query)[0].gesundheitsamt_url;
}

var getDiviValue = function(diviMeasurementType, date, kreis_id = 14) {

  const query = `SELECT value 
    FROM measurement m, kreise_sachsen k 
    WHERE type = '${diviMeasurementType}' 
    and date::date = '${moment(date).format("YYYY-MM-DD")}'
    and k.id = m.kreis_id 
    and m.kreis_id = ${kreis_id} 
    LIMIT 1;`
    
  return client.querySync(query)[0] ? client.querySync(query)[0].value : null;
}

var getKreisIdForAgs = function(ags) {

  const query = `SELECT kreis_id FROM gemeinden WHERE ags = '${ags}'`
  return client.querySync(query)[0]["kreis_id"];
}

var getGemeindeIdForAgs = function(ags) {

  const query = `SELECT id FROM gemeinden WHERE ags = '${ags}'`
  return client.querySync(query)[0]["id"];
}

var getGemeindeIdForName = function(name, kreis_id) {

  const query = `SELECT * FROM gemeinden WHERE gemeinde = '${name}' and kreis_id = ${kreis_id}`
  return client.querySync(query)[0];
}


var getValueForGemeindeById = function(id, type = "7d-infections", source = 'sms', limit = 1, offset = 0) {

  const query = `SELECT * FROM measurement WHERE gemeinde_id = ${id} and type = '${type}' and source = '${source}' ORDER BY date desc LIMIT ${limit} OFFSET ${offset};`
  return client.querySync(query);
}

var execQuery = function(query) {

  return client.querySync(query);
}

var has7dIncidenceForDate = function(kreis_id, date) {
  const query = `SELECT * FROM measurement m
    WHERE type = '7d-incidence'
      AND m.kreis_id = ${kreis_id}
      AND date = '${date}'
      AND m.source = 'rki';`
  return client.querySync(query).length > 0;
}

var insertKreisValue = function(kreis_id, type, value, date, source = "rki"){
  client.querySync(
    `INSERT INTO public.measurement ("kreis_id", "date", "source", "type", "value", "gemeinde_id") 
     VALUES (${kreis_id}, '${date}', '${source}', '${type}', ${value}, 0);`
  )
}

var getHospitalizedForDate = function(date, type = 'numberOfOccupiedBeds'){

  const query = `SELECT value FROM measurement m
    WHERE kreis_id = 14
    AND type = '${type}'
    AND m.source = 'sms'
    AND date::date = '${date.format("YYYY-MM-DD")}'
    LIMIT 1;`;

  var result = { 
    numberOfOccupiedBeds : client.querySync(query)[0] ? client.querySync(query)[0].value : undefined
  };

  const query2 = `SELECT value FROM measurement m
    WHERE kreis_id = 14
    AND type = 'numberOfOccupiedItsBeds'
    AND m.source = 'sms'
    AND date::date = '${date.format("YYYY-MM-DD")}'
    LIMIT 1;`;

  result.numberOfOccupiedItsBeds = client.querySync(query2)[0] ? client.querySync(query2)[0].value : undefined
  result.numberOfCovidOnNormalStation = result.numberOfOccupiedBeds - result.numberOfOccupiedItsBeds;

  return result;
}

var getRValue = function(){
  return {
    r_t : client.querySync(`SELECT value FROM measurement m WHERE kreis_id = 14 AND type = 'r_t' AND m.source = 'rtlive' ORDER BY date desc LIMIT 1;`)[0].value,
    r_t_lower : client.querySync(`SELECT value FROM measurement m WHERE kreis_id = 14 AND type = 'r_t_lower' AND m.source = 'rtlive' ORDER BY date desc LIMIT 1;`)[0].value,
    r_t_upper : client.querySync(`SELECT value FROM measurement m WHERE kreis_id = 14 AND type = 'r_t_upper' AND m.source = 'rtlive' ORDER BY date desc LIMIT 1;`)[0].value,
    r_t_threshold_probability : client.querySync(`SELECT value FROM measurement m WHERE kreis_id = 14 AND type = 'r_t_threshold_probability' AND m.source = 'rtlive' ORDER BY date desc LIMIT 1;`)[0].value
  }
} 

var getGemeinden = function(){
  return client.querySync("SELECT * FROM gemeinden WHERE id != 0;")
}

var getMeasurementForGemeinde = function(gemeinde_id, type){
  return client.querySync(`SELECT * FROM measurement WHERE gemeinde_id = ${gemeinde_id} AND type = '${type}' ORDER BY date asc;`)
}

var getVaccinations = function(kreis_id, typeRegex) {
  const query = `SELECT * FROM measurement WHERE kreis_id = ${kreis_id} AND type ~ '${typeRegex}' ORDER BY date DESC, id;`;
  return client.querySync(query)
}

var getVaccinationManufacturers = function() {
  return client.querySync(`
    SELECT type, SUM (value)
    FROM measurement
    WHERE type ~ 'delivery-'
    GROUP BY type
    ORDER BY SUM (value) DESC;`)
}

var insertGemeindeValue = function(kreis_id, date, type, value, gemeinde_id) {

  const query = 
  `INSERT INTO "public"."measurement"("kreis_id","date","source","value","type","gemeinde_id")
    VALUES (${kreis_id},'${date.format("YYYY-MM-DD")}','sms',${value},'${type}',${gemeinde_id})
    ON CONFLICT(kreis_id, date, type, source, gemeinde_id) DO UPDATE SET value = EXCLUDED.value, delta_yesterday = EXCLUDED.delta_yesterday;`

  client.querySync(query);
 }

module.exports.insertGemeindeValue = insertGemeindeValue
module.exports.getVaccinationManufacturers = getVaccinationManufacturers
module.exports.getVaccinations = getVaccinations
module.exports.getMeasurementForGemeinde = getMeasurementForGemeinde
module.exports.getGemeinden = getGemeinden
module.exports.getRValue = getRValue
module.exports.getHospitalizedForDate = getHospitalizedForDate
module.exports.get7dIncidenceForKreisOneWeekBefore = get7dIncidenceForKreisOneWeekBefore
module.exports.get7dIncidenceForKreisToday = get7dIncidenceForKreisToday
module.exports.get7dIncidenceForKreisYesterday = get7dIncidenceForKreisYesterday
module.exports.getXDaysIncidenceForDistrictOnDate = getXDaysIncidenceForDistrictOnDate;
module.exports.insertKreisValue = insertKreisValue;
module.exports.has7dIncidenceForDate = has7dIncidenceForDate;
module.exports.getGemeindeIdForName = getGemeindeIdForName;
module.exports.getValueForGemeindeById = getValueForGemeindeById;
module.exports.execQuery = execQuery;
module.exports.getKreisIdForAgs = getKreisIdForAgs;
module.exports.getGemeindeIdForAgs = getGemeindeIdForAgs;
module.exports.getWeekValues = getWeekValues;
module.exports.getCurrentCovidValueFor = getCurrentCovidValueFor;
module.exports.getDiviValue = getDiviValue;
module.exports.getTotalDeaths = getTotalDeaths;
module.exports.getTotalInfections = getTotalInfections;
module.exports.getHealthOfficeUrl = getHealthOfficeUrl;
module.exports.getTotalDeathsForDistrict = getTotalDeathsForDistrict;
module.exports.getInfectionsSumForLastXDays = getInfectionsSumForLastXDays;
module.exports.getMaxDate = getMaxDate;
module.exports.getInfectionsForDistrictAndDate = getInfectionsForDistrictAndDate;
module.exports.getInfectionBetweenDates = getInfectionBetweenDates;
module.exports.getInfectionsForDistrict = getInfectionsForDistrict;
module.exports.getXDaysIncidenceForDistrict = getXDaysIncidenceForDistrict;
module.exports.getCurrentCovidCases = getCurrentCovidCases;
module.exports.getCurrentCovidCasesVentilated = getCurrentCovidCasesVentilated;
module.exports.getTotalInfectionsForDistrict = getTotalInfectionsForDistrict;
module.exports.getInfectionsForDays = getInfectionsForDays;
module.exports.getSingleCovidValueFor = getSingleCovidValueFor;
