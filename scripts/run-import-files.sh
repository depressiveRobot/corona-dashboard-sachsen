#!/bin/bash
set -e

currentDir="$(cd "$(dirname "$1")"; pwd -P)/$(basename "$1")"
POSTGRES="PGUSER=${PGUSER:=corona} PGHOST=${PGHOST:=localhost} PGPASSWORD=${PGPASSWORD} PGDATABASE=${PGDATABASE:=corona} PGPORT=${PGPORT:=5432}"
DOWNLOAD="${DOWNLOAD:=false}"

if [ "$DOWNLOAD" = true ] ; then
  wget https://www.arcgis.com/sharing/rest/content/items/f10774f1c63e40168479a1feb6c7ca74/data -O dist/RKI_COVID19_full.csv
  grep -E ",14,Sachsen,|,IdBundesland," dist/RKI_COVID19_full.csv > dist/RKI_COVID19.csv

  wget https://raw.githubusercontent.com/jgehrcke/covid-19-germany-gae/master/cases-rl-crowdsource-by-ags.csv -O dist/risklayer_COVID19.csv
  
  wget https://www.gstatic.com/covid19/mobility/Global_Mobility_Report.csv -O dist/google-mobility.csv
  grep -E "DE,Germany,Saxony,|country_region_code" dist/google-mobility.csv > dist/google-mobility-sn.csv

  wget "https://docs.google.com/spreadsheets/d/1G-2TAslg1KOOlHayxVd8WMEia2wln02j_OerBHZjpnA/export?format=csv" -O dist/covid-mutanten.csv

  wget https://impfdashboard.de/static/data/germany_deliveries_timeseries_v2.tsv -O dist/germany_deliveries_timeseries_v2.tsv
fi

STARTTIME=$(date +%s)
echo "Preprocessing RKI files"
env $POSTGRES node scripts/preprocess/process-rki-csv.js
ENDTIME=$(date +%s)
echo "It took $(($ENDTIME - $STARTTIME)) seconds to complete this task..."

STARTTIME=$(date +%s)
echo "Preprocessing Risklayer files"
env $POSTGRES node scripts/preprocess/process-risklayer-csv.js
ENDTIME=$(date +%s)
echo "It took $(($ENDTIME - $STARTTIME)) seconds to complete this task..."

STARTTIME=$(date +%s)
echo "Importing RKI files"
env $POSTGRES node scripts/import/import-rki.js
ENDTIME=$(date +%s)
echo "It took $(($ENDTIME - $STARTTIME)) seconds to complete this task..."

STARTTIME=$(date +%s)
echo "Importing Risklayer files"
env $POSTGRES node scripts/import/import-risklayer.js
ENDTIME=$(date +%s)
echo "It took $(($ENDTIME - $STARTTIME)) seconds to complete this task..."

STARTTIME=$(date +%s)
echo "Importing DIVI data"
env $POSTGRES node scripts/import/import-hospitals-divi.js 
ENDTIME=$(date +%s)
echo "It took $(($ENDTIME - $STARTTIME)) seconds to complete this task..."

STARTTIME=$(date +%s)
echo "Importing SMS hospital data"
env $POSTGRES node scripts/import/import-hospitals-sms.js
ENDTIME=$(date +%s)
echo "It took $(($ENDTIME - $STARTTIME)) seconds to complete this task..."

STARTTIME=$(date +%s)
echo "Importing Gemeinde date from SMS"
env $POSTGRES node scripts/import/import-gemeinden.js
ENDTIME=$(date +%s)
echo "It took $(($ENDTIME - $STARTTIME)) seconds to complete this task..."

STARTTIME=$(date +%s)
echo "Importing R value from rtlive.de"
env $POSTGRES node scripts/import/import-juelich-r.js
ENDTIME=$(date +%s)
echo "It took $(($ENDTIME - $STARTTIME)) seconds to complete this task..."

STARTTIME=$(date +%s)
echo "Importing daily deaths from RKI"
env $POSTGRES node scripts/import/import-daily-deaths.js
ENDTIME=$(date +%s)
echo "It took $(($ENDTIME - $STARTTIME)) seconds to complete this task..."

STARTTIME=$(date +%s)
echo "Importing vaccinations from SMS"
env $POSTGRES node scripts/import/import-sms-impfung.js
ENDTIME=$(date +%s)
echo "It took $(($ENDTIME - $STARTTIME)) seconds to complete this task..."

STARTTIME=$(date +%s)
echo "Importing vaccinations delivery data from RKI"
env $POSTGRES node scripts/import/import-rki-impfung.js
ENDTIME=$(date +%s)
echo "It took $(($ENDTIME - $STARTTIME)) seconds to complete this task..."