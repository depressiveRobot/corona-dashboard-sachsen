const replace = require('replace-in-file');
const parse = require('csv-parse/lib/sync');
const fs = require('fs');
const _ = require('lodash');
const moment = require('moment');
var helper = require('./helper.js');
let path = require("path");
const commons = require('./replacements/commons');

async function getRows(path){

  // Read the content
  const content = await fs.promises.readFile(path)
  // Parse the CSV content
  const rows = parse(content, {columns : true})
  return rows;
}

function getVacinationRows(path){
  return getRows(path);
}

async function getRkiRows(){
  return getRows(`dist/RKI_COVID19.csv`);
}


function getDistributionInAgeGroup(rows, ageGroup, gender, kreis_id, days, offset = 0){

  var infectionsPerAgePerGender = rows.filter(item => 
      item.Geschlecht.match(gender) && 
      item.Altersgruppe == ageGroup);

  if ( kreis_id != 14 )
    infectionsPerAgePerGender = infectionsPerAgePerGender.filter(item => item.IdLandkreis == commons.kreisIdToAgsMapping[kreis_id]);

  if ( days )
    infectionsPerAgePerGender = infectionsPerAgePerGender.filter(item => 
      moment(item.Meldedatum.replace(" 00:00:00", ""), "YYYY/MM/DD")
        .isBetween(
          moment().subtract(days + offset, 'days'),
          moment().subtract(offset, 'days'), undefined, '[]'))

  return infectionsPerAgePerGender.reduce((accumulator, landkreisValue) => accumulator + parseInt(landkreisValue.AnzahlFall), 0);
}

module.exports.getRows = getRows;
module.exports.getRkiRows = getRkiRows;
module.exports.getVacinationRows = getVacinationRows;
module.exports.getDistributionInAgeGroup = getDistributionInAgeGroup;
