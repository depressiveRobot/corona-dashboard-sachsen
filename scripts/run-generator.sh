#!/bin/bash

# save the postgresql connection settings in one variable
POSTGRES="PGUSER=${PGUSER:=corona} PGHOST=${PGHOST:=localhost} PGPASSWORD=${PGPASSWORD} PGDATABASE=${PGDATABASE:=corona} PGPORT=${PGPORT:=5432}"

# copy a dist file from the template
cp html/index.template.html dist/index_in_progress.html
mkdir -p dist/json/ 
mkdir -p dist/r/
mkdir -p dist/images/
mkdir -p dist/covidsim/
cp -f html/json/sachsen.js dist/json/sachsen.js
cp -f html/covidsim/cosim-Sachsen-current.csv dist/covidsim/cosim-Sachsen-current.csv
cp -f html/images/impfbereitschaft_deutschland.png dist/images/impfbereitschaft_deutschland.png

printf "%-05s%-35s%-20s\n" "###" "Generator" "Time in seconds"
printf "%-05s%-35s%-20s\n" "---" "--------------------------------" "---------"
TOTALSTARTTIME=$(date +%s)

# run all the replacements one after the other
STARTTIME=$(date +%s)
env $POSTGRES node scripts/replacements/001_saxony_general.js "$@"
ENDTIME=$(date +%s)
printf "%-05s%-35s%-20s\n" "001" "saxony_general" "$(($ENDTIME - $STARTTIME))s"

STARTTIME=$(date +%s)
env $POSTGRES node scripts/replacements/002_maps.js "$@"
ENDTIME=$(date +%s)
printf "%-05s%-35s%-20s\n" "002" "maps" "$(($ENDTIME - $STARTTIME))s"

STARTTIME=$(date +%s)
env $POSTGRES node scripts/replacements/003_yesterday_diff.js "$@"
ENDTIME=$(date +%s)
printf "%-05s%-35s%-20s\n" "003" "yesterday_diff" "$(($ENDTIME - $STARTTIME))s"

STARTTIME=$(date +%s)
env $POSTGRES node scripts/replacements/004_top_events.js "$@"
ENDTIME=$(date +%s)
printf "%-05s%-35s%-20s\n" "004" "top_events" "$(($ENDTIME - $STARTTIME))s"

STARTTIME=$(date +%s)
env $POSTGRES node scripts/replacements/005_kreis_template.js "$@"
ENDTIME=$(date +%s)
printf "%-05s%-35s%-20s\n" "005" "kreis_template" "$(($ENDTIME - $STARTTIME))s"

STARTTIME=$(date +%s)
env $POSTGRES node scripts/replacements/006_hospitals.js "$@"
ENDTIME=$(date +%s)
printf "%-05s%-35s%-20s\n" "006" "hospitals" "$(($ENDTIME - $STARTTIME))s"

STARTTIME=$(date +%s)
env $POSTGRES node scripts/replacements/007_most_important_info.js "$@"
ENDTIME=$(date +%s)
printf "%-05s%-35s%-20s\n" "007" "most_important_info" "$(($ENDTIME - $STARTTIME))s"

STARTTIME=$(date +%s)
env $POSTGRES node scripts/replacements/008_relevant_links.js "$@"
ENDTIME=$(date +%s)
printf "%-05s%-35s%-20s\n" "008" "relevant_links" "$(($ENDTIME - $STARTTIME))s"

STARTTIME=$(date +%s)
env $POSTGRES node scripts/replacements/010_mobility.js "$@"
ENDTIME=$(date +%s)
printf "%-05s%-35s%-20s\n" "010" "mobility" "$(($ENDTIME - $STARTTIME))s"

STARTTIME=$(date +%s)
env $POSTGRES node scripts/replacements/011_age_incidence.js "$@"
ENDTIME=$(date +%s)
printf "%-05s%-35s%-20s\n" "011" "age_incidence" "$(($ENDTIME - $STARTTIME))s"

STARTTIME=$(date +%s)
env $POSTGRES node scripts/replacements/012_divi_graph.js "$@"
ENDTIME=$(date +%s)
printf "%-05s%-35s%-20s\n" "012" "divi_graph" "$(($ENDTIME - $STARTTIME))s"

STARTTIME=$(date +%s)
env $POSTGRES node scripts/replacements/013_cases.js "$@"
ENDTIME=$(date +%s)
printf "%-05s%-35s%-20s\n" "013" "cases" "$(($ENDTIME - $STARTTIME))s"

STARTTIME=$(date +%s)
env $POSTGRES node scripts/replacements/014_circles.js "$@"
ENDTIME=$(date +%s)
printf "%-05s%-35s%-20s\n" "014" "circles" "$(($ENDTIME - $STARTTIME))s"

STARTTIME=$(date +%s)
env $POSTGRES node scripts/replacements/015_age_gender_distribution.js "$@"
ENDTIME=$(date +%s)
printf "%-05s%-35s%-20s\n" "015" "age_gender_distribution" "$(($ENDTIME - $STARTTIME))s"

STARTTIME=$(date +%s)
env $POSTGRES node scripts/replacements/016_divi_kreise.js "$@"
ENDTIME=$(date +%s)
printf "%-05s%-35s%-20s\n" "016" "divi_kreise" "$(($ENDTIME - $STARTTIME))s"

STARTTIME=$(date +%s)
env $POSTGRES node scripts/replacements/017_infection_date_distribution.js "$@"
ENDTIME=$(date +%s)
printf "%-05s%-35s%-20s\n" "017" "infection_date_distribution" "$(($ENDTIME - $STARTTIME))s"

STARTTIME=$(date +%s)
env $POSTGRES node scripts/replacements/018_week_updates.js "$@"
ENDTIME=$(date +%s)
printf "%-05s%-35s%-20s\n" "018" "week_updates" "$(($ENDTIME - $STARTTIME))s"

STARTTIME=$(date +%s)
env $POSTGRES node scripts/replacements/020_gemeinden.js "$@"
ENDTIME=$(date +%s)
printf "%-05s%-35s%-20s\n" "020" "gemeinden" "$(($ENDTIME - $STARTTIME))s"

STARTTIME=$(date +%s)
env $POSTGRES node scripts/replacements/021_gemeinden_table.js "$@"
ENDTIME=$(date +%s)
printf "%-05s%-35s%-20s\n" "021" "gemeinden_table" "$(($ENDTIME - $STARTTIME))s"

STARTTIME=$(date +%s)
env $POSTGRES node scripts/replacements/024_days_below_incidence.js "$@"
ENDTIME=$(date +%s)
printf "%-05s%-35s%-20s\n" "024" "days_below_incidence" "$(($ENDTIME - $STARTTIME))s"

STARTTIME=$(date +%s)
env $POSTGRES node scripts/replacements/025_hospital_beds.js "$@"
ENDTIME=$(date +%s)
printf "%-05s%-35s%-20s\n" "025" "hospital_beds" "$(($ENDTIME - $STARTTIME))s"

STARTTIME=$(date +%s)
env $POSTGRES node scripts/replacements/026_age_infection_development.js "$@"
ENDTIME=$(date +%s)
printf "%-05s%-35s%-20s\n" "026" "age_infection_development" "$(($ENDTIME - $STARTTIME))s"

STARTTIME=$(date +%s)
env $POSTGRES node scripts/replacements/027_growth.js "$@"
ENDTIME=$(date +%s)
printf "%-05s%-35s%-20s\n" "027" "growth" "$(($ENDTIME - $STARTTIME))s"

STARTTIME=$(date +%s)
env $POSTGRES node scripts/replacements/028_covidsim.js "$@"
ENDTIME=$(date +%s)
printf "%-05s%-35s%-20s\n" "028" "covidsim" "$(($ENDTIME - $STARTTIME))s"

STARTTIME=$(date +%s)
env $POSTGRES node scripts/replacements/029_gemeinden_data_output.js "$@"
ENDTIME=$(date +%s)
printf "%-05s%-35s%-20s\n" "029" "gemeinden_data_output" "$(($ENDTIME - $STARTTIME))s"

STARTTIME=$(date +%s)
env $POSTGRES node scripts/replacements/030_vaccine_manufacturer.js "$@"
ENDTIME=$(date +%s)
printf "%-05s%-35s%-20s\n" "030" "vaccine_manufacturer" "$(($ENDTIME - $STARTTIME))s"

STARTTIME=$(date +%s)
env $POSTGRES node scripts/replacements/031_gemeinde_chart.js "$@"
ENDTIME=$(date +%s)
printf "%-05s%-35s%-20s\n" "031" "gemeinde_chart.js" "$(($ENDTIME - $STARTTIME))s"

TOTALENDTIME=$(date +%s)
printf "%-05s%-35s%-20s\n" "---" "--------------------------------" "---------"
printf "%-05s%-35s%-20s\n" "" "Total" "$(($TOTALENDTIME - $TOTALSTARTTIME))s"
# copy the temporary template file to the dist folder
mv dist/index_in_progress.html dist/index.html