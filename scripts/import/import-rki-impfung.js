// Fetch a URL and parse all it's tables into JSON, using promises
var tabletojson = require('tabletojson').Tabletojson;
var url = 'https://impfdashboard.de/static/data/germany_deliveries_timeseries_v2.tsv';
const jsdom = require("jsdom");
const moment = require('moment');
const parse = require('csv-parse/lib/sync');
const fs = require('fs');
const path = require('path');
const https = require('https');
moment.locale("de_DE");
const { JSDOM } = jsdom;
// sync/blocking mode
var Client = require('pg-native')
var client = new Client()
client.connectSync()

const { program } = require('commander');

program
  .option('-d, --debug', 'output extra debugging')
  .option('-t, --tablename <type>', 'the name of the source database table name, used for debugging', 'measurement')
  .option('-f, --file <type>', 'an input file to process')
program.parse(process.argv);

if (program.debug) {
	console.log(program.opts());
}

const insertData = function(date, type, value, kreis_id = 14) {

  client.querySync(`
    INSERT INTO "measurement"("kreis_id","date","source","value","type","gemeinde_id")
    VALUES (${kreis_id},'${date.format("YYYY-MM-DD")}'::date,'sms',${value},'${type}', 0)
    ON CONFLICT(kreis_id, date, type, source, gemeinde_id) DO UPDATE SET value = EXCLUDED.value, delta_yesterday = EXCLUDED.delta_yesterday`);
}

async function importData(){

  // Read the content
  const content = await fs.promises.readFile(`dist/germany_deliveries_timeseries_v2.tsv`)
  // Parse the CSV content
  const records = parse(content, {columns : true, delimiter : "\t"})
  const sachsen = records.filter(row => row.region == 'DE-SN');
  sachsen.forEach(row => {
    insertData(moment(row.date), "delivery-" + row.impfstoff, row.dosen, 14);
  })
}

importData();


