// sync/blocking mode
const moment = require('moment');
var Client = require('pg-native')
var client = new Client()
client.connectSync()

const { program } = require('commander');

program
  .option('-d, --debug', 'output extra debugging')
  .option('-t, --tablename <type>', 'the name of the source database table name, used for debugging', 'measurement')
  .option('-f, --file <type>', 'an input file to process')
program.parse(process.argv);

var rows = client.querySync(`SELECT * FROM "public"."${program.tablename}"`);
rows.forEach(function(measurement){

  query = `SELECT * 
    FROM ${program.tablename} 
    WHERE kreis_id = ${measurement.kreis_id} 
      AND date::date = '${moment(measurement.date, "DD. MMMM YYYY, HH:mm z").format()}'::date - integer '1' 
      AND type = '${measurement.type}';`

  prev_measurement = client.querySync(query)[0];
  if (prev_measurement) {

    client.querySync(
      `UPDATE ${program.tablename} 
       SET previous_measurement_id = ${prev_measurement.id} 
       WHERE id = ${measurement.id};`);
  }
})