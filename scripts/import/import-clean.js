// sync/blocking mode
const moment = require('moment');
var Client = require('pg-native')
var client = new Client()
client.connectSync()

const { program } = require('commander');

program
  .option('-d, --debug', 'output extra debugging')
  .option('-t, --tablename <type>', 'the name of the source database table name, used for debugging', 'measurement')
  .option('-f, --file <type>', 'an input file to process')
program.parse(process.argv);

client.querySync(`DELETE FROM "public"."${program.tablename}"`);