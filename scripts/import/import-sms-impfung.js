// Fetch a URL and parse all it's tables into JSON, using promises
var tabletojson = require('tabletojson').Tabletojson;
var url = 'https://www.coronavirus.sachsen.de/ueberblick-coronaschutzimpfungen-in-sachsen-9874.html';
const jsdom = require("jsdom");
const moment = require('moment');
const fs = require('fs');
const path = require('path');
moment.locale("de_DE");
const { JSDOM } = jsdom;
// sync/blocking mode
var Client = require('pg-native')
var client = new Client()
client.connectSync()

const { program } = require('commander');

program
  .option('-d, --debug', 'output extra debugging')
  .option('-t, --tablename <type>', 'the name of the source database table name, used for debugging', 'measurement')
  .option('-f, --file <type>', 'an input file to process')
  .option('--directory <type>', 'an unput directory')
program.parse(process.argv);

if (program.debug) {
	console.log(program.opts());
}

const insertData = function(date, type, value, kreis_id = 14) {

  client.querySync(`
    INSERT INTO "measurement"("kreis_id","date","source","value","type","gemeinde_id")
    VALUES (${kreis_id},'${date.format("YYYY-MM-DD")}'::date,'sms',${value},'${type}', 0)
    ON CONFLICT(kreis_id, date, type, source, gemeinde_id) DO UPDATE SET value = EXCLUDED.value, delta_yesterday = EXCLUDED.delta_yesterday`);
}

var mappingKreise = {
	'Landeshauptstadt Dresden' : 1,
  'Stadt Dresden' : 1,
	'Stadt Chemnitz' : 2,
	'Stadt Leipzig' : 3,
	'Landkreis Bautzen' : 4,
	'Erzgebirgskreis' : 5,
	'Landkreis Görlitz' : 6,
  'LK Görlitz' : 6,
	'Landkreis Leipzig' : 7,
  'LK Leipzig' : 7,
	'Landkreis Meißen' : 8,
  'LK Meißen' : 8,
	'Landkreis Mittelsachsen' : 9,
  'LK Mittelsachsen': 9,
	'Landkreis Nordsachsen' : 10,
  'LK Nordsachsen' : 10,
	'Landkreis Sächsische Schweiz-Osterzgebirge' : 11,
  'Landkreis Sächsische Schweiz Osterzgebirge' : 11,
  'LK Sächs. Schweiz Osterzgeb' : 11,
	'Vogtlandkreis' : 12,
  'IZ Plauen': 15,
  'Impfzentrum Plauen': 15,
	'Landkreis Zwickau': 13,
  'LK Zwickau' : 13,
	'Gesamtzahl': 14,
  'Gesamtzahl der Infektionen': 14,
  'Sachsen gesamt' : 14,
  'Arztpraxen' : 14,
  'Pilotprojekt Hausärzte' : 14
}

var parseData = function(tablesAsJson, dom) {

  let erstimpfungen = -1, zweitimpfungen = -1, erstimpfungenTotal = -1, zweitimpfungenTotal = -1, date = '';

  dom.window.document.querySelectorAll("p").forEach(function(p){
    
    if ( p.textContent.includes("Stand: ") ) {
      date = moment(p.textContent.replace("Stand: ", "").replace(/\|.*/g, "").trim(), "D. MMMM YYYY");
    }
  });

  dom.window.document.querySelectorAll("li").forEach(function(li){

    // Erstimpfungen an diesem Tag: 31.414
    // Zweitimpfungen an diesem Tag: 2.924
    // Erstimpfung 529.022
    // Zweitimpfungen 280.395
    if (li.textContent.includes("Erstimpfungen an diesem Tag:")) {
      erstimpfungen = Number(li.textContent.replace("Erstimpfungen an diesem Tag:", "").trim().replace(/\./g, ""));
    }
    if (li.textContent.includes("Zweitimpfungen an diesem Tag:")) {
      zweitimpfungen = Number(li.textContent.replace("Zweitimpfungen an diesem Tag:", "").trim().replace(/\./g, ""));
    }

    if (li.textContent.includes("Erstimpfung ")) {
      erstimpfungenTotal = Number(li.textContent.replace("Erstimpfung ", "").trim().replace(/\./g, ""));
    }
    if (li.textContent.includes("Zweitimpfungen ")) {
      zweitimpfungenTotal = Number(li.textContent.replace("Zweitimpfungen ", "").trim().replace(/\./g, ""));
    }    
  });

  // console.log(date, erstimpfungen, zweitimpfungen, erstimpfungenTotal, zweitimpfungenTotal)

  insertData(date, 'first-vaccination-today', erstimpfungen);
  insertData(date, 'second-vaccination-today', zweitimpfungen);
  insertData(date, 'first-vaccination-total', erstimpfungenTotal);
  insertData(date, 'second-vaccination-total', zweitimpfungenTotal);
  
  tablesAsJson[0].forEach(row => {
    
    const name = row['0'];
    const kreis_id = mappingKreise[name];

    if ( kreis_id < 14 ) {

      let firstVaccinationTotal  = Number(row['Anzahl Erstimpfung'].replace(/\./g, ""));
      let secondVaccinationTotal = Number(row['Anzahl Zweitimpfung'].replace(/\./g, ""));

      insertData(date, 'first-vaccination-total', firstVaccinationTotal, kreis_id);
      insertData(date, 'second-vaccination-total', secondVaccinationTotal, kreis_id);
    } 
  });
}

if ( program.file ) {
	const html = fs.readFileSync(path.resolve(__dirname, program.file), {encoding: 'UTF-8'});
  parseData(tabletojson.convert(html), new JSDOM(html));
}
else if ( program.directory ) {

  const dir = fs.opendirSync(program.directory);
  let dirent;
  while ((dirent = dir.readSync()) !== null) {
    const html = fs.readFileSync(program.directory + "/" + dirent.name, {encoding: 'UTF-8'});
    parseData(tabletojson.convert(html), new JSDOM(html));
  }
  dir.closeSync()
}
else {
	tabletojson.convertUrl(url).then(function(tablesAsJson) {
    JSDOM.fromURL(url).then(dom => {
      parseData(tablesAsJson, dom);
    });
  })
}
