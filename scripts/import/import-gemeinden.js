const moment = require('moment');
var https = require('https');
moment.locale("de_DE");
const { JSDOM } = require("jsdom");
const parse = require('csv-parse/lib/sync');
const fs = require('fs');
const path = require('path');
const _ = require('lodash');
var sql = require('../../scripts/sql.js');
var tabletojson = require('tabletojson').Tabletojson;
const { xor } = require('lodash');
const commons = require('../replacements/commons.js');
const pathToDir = process.argv.slice(2)[0];
const url = `https://www.coronavirus.sachsen.de/infektionsfaelle-in-sachsen-4151.html`;
const urls = {
  4  : '/corona-statistics/rest/communitySituation.jsp?boundaryId=14625',
  5  : '/corona-statistics/rest/communitySituation.jsp?boundaryId=14521',
  6  : '/corona-statistics/rest/communitySituation.jsp?boundaryId=14626',
  7  : '/corona-statistics/rest/communitySituation.jsp?boundaryId=14729',
  8  : '/corona-statistics/rest/communitySituation.jsp?boundaryId=14627',
  9  : '/corona-statistics/rest/communitySituation.jsp?boundaryId=14522',
  10 : '/corona-statistics/rest/communitySituation.jsp?boundaryId=14730',
  11 : '/corona-statistics/rest/communitySituation.jsp?boundaryId=14628',
  12 : '/corona-statistics/rest/communitySituation.jsp?boundaryId=14523',
  13 : '/corona-statistics/rest/communitySituation.jsp?boundaryId=14524'
};

function importDataFromFile(path) {

  const directory = fs.opendirSync(path);
  let entry;
  while ((entry = directory.readSync()) !== null) {
    
    const names = entry.name.replace(".html", "").split("_");
    const date = moment(`${names[3]}-${names[1]}-${names[2]}`);
    const kreis_id = commons.agsToKreisIdMapping[names[0]]

    const gemeindenData = [];
    gemeindenData.push({ name: "Dresden",  infections : 0, population : commons.population[1], kreis_id : 1, id: 497 });
    gemeindenData.push({ name: "Leipzig",  infections : 0, population : commons.population[3], kreis_id : 3, id: 615 });
    gemeindenData.push({ name: "Chemnitz", infections : 0, population : commons.population[2], kreis_id : 2, id: 471 });

    const tablesAsJson = tabletojson.convert(fs.readFileSync(path + entry.name, {encoding: 'UTF-8'})); 

    tablesAsJson[0].forEach(item => {
      gemeindenData.push({
        name : item.Gemeinde,
        infections : item['Neuzugänge letzten 7 Tage'].replace("*",""),
        incidence : item['7-Tages-Inzidenz'].replace("*","").replace(".", "").replace(",", "."),
        population : item.Einwohner,
        kreis_id : kreis_id
      })
    })

    const dateDiff = moment().diff(date, 'days')
    console.log(`Importing ${gemeindenData.length} gemeinden from SMS data for ${date.format("DD.MM.YYYY")} with diff of: ${dateDiff} days`)
    gemeindenData[0].incidence = Number(sql.getXDaysIncidenceForDistrict(1, 7 + dateDiff, 2, dateDiff))
    gemeindenData[1].incidence = Number(sql.getXDaysIncidenceForDistrict(3, 7 + dateDiff, 2, dateDiff))
    gemeindenData[2].incidence = Number(sql.getXDaysIncidenceForDistrict(2, 7 + dateDiff, 2, dateDiff))

    // sometimes there are empty rows
    gemeindenData.filter(gemeinde => gemeinde.name).forEach(gemeinde => {

      // not all gemeinden match the name in the DB
      var name = gemeinde.name.replace(", Stadt", "").replace(", Kurort", "")
        .replace(", Universitätsstadt", "").replace(", Hochschulstadt", "").replace(" (Erzgebirgskreis)", "")
        .replace(",\n                          Stadt (Landkreis Bautzen)", "")
        .replace(" (Landkreis Zwickau)", "").replace("/Lausitz", "")
        .replace(",\n\t\t\tStadt (Landkreis Bautzen)", "").replace(",\nStadt (Landkreis Bautzen)", "")
      
      gemeinde.name = name;
      gemeinde.id = gemeinde.id ? gemeinde.id : sql.getGemeindeIdForName(name, gemeinde.kreis_id).id;

      sql.insertGemeindeValue(gemeinde.kreis_id, date, "7d-incidence", gemeinde.incidence, gemeinde.id);
      sql.insertGemeindeValue(gemeinde.kreis_id, date, "7d-infections", gemeinde.infections, gemeinde.id);          
    });
  }

  directory.closeSync();
}

if ( pathToDir ) {
  importDataFromFile(pathToDir)
} 
else {

  var date = undefined;

  for (const [kreis_id, tableUrl] of Object.entries(urls)) {

    var req = https.request({method: 'HEAD', host: "www.coronavirus.sachsen.de", path : tableUrl }, function(res) {
      
      date = moment(res.headers['last-modified']);
      const gemeindenData = [];
      gemeindenData.push({ name: "Dresden",  infections : 0, population : commons.population[1], kreis_id : 1, id: 497 });
      gemeindenData.push({ name: "Leipzig",  infections : 0, population : commons.population[3], kreis_id : 3, id: 615 });
      gemeindenData.push({ name: "Chemnitz", infections : 0, population : commons.population[2], kreis_id : 2, id: 471 });

      tabletojson.convertUrl("https://www.coronavirus.sachsen.de" + tableUrl).then(function(tablesAsJson) {  
        tablesAsJson[0].forEach(item => {
          gemeindenData.push({
            name : item.Gemeinde,
            infections : item['Neuzugänge letzten 7 Tage'].replace("*",""),
            incidence : item['7-Tages-Inzidenz'].replace("*","").replace(".", "").replace(",", "."),
            population : item.Einwohner,
            kreis_id : kreis_id
          })
        })

        const dateDiff = moment().diff(date, 'days')
        console.log(`Importing ${gemeindenData.length} gemeinden from SMS data for ${date.format("DD.MM.YYYY")} with diff of: ${dateDiff} days`)
        gemeindenData[0].incidence = Number(sql.getXDaysIncidenceForDistrict(1, 7 + dateDiff, 2, dateDiff))
        gemeindenData[1].incidence = Number(sql.getXDaysIncidenceForDistrict(3, 7 + dateDiff, 2, dateDiff))
        gemeindenData[2].incidence = Number(sql.getXDaysIncidenceForDistrict(2, 7 + dateDiff, 2, dateDiff))

        // sometimes there are empty rows
        gemeindenData.filter(gemeinde => gemeinde.name).forEach(gemeinde => {

          // not all gemeinden match the name in the DB
          var name = gemeinde.name.replace(", Stadt", "").replace(", Kurort", "")
            .replace(", Universitätsstadt", "").replace(", Hochschulstadt", "").replace(" (Erzgebirgskreis)", "")
            .replace(",\n                          Stadt (Landkreis Bautzen)", "")
            .replace(" (Landkreis Zwickau)", "").replace("/Lausitz", "")
            .replace(",\n\t\t\tStadt (Landkreis Bautzen)", "").replace(",\nStadt (Landkreis Bautzen)", "")
          
          gemeinde.name = name;
          gemeinde.id = gemeinde.id ? gemeinde.id : sql.getGemeindeIdForName(name, gemeinde.kreis_id).id;

          sql.insertGemeindeValue(gemeinde.kreis_id, date, "7d-incidence", gemeinde.incidence, gemeinde.id);
          sql.insertGemeindeValue(gemeinde.kreis_id, date, "7d-infections", gemeinde.infections, gemeinde.id);          
        })
      })
    });

    req.end();
  }
}