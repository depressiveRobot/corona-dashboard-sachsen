const moment = require('moment');
moment.locale("de_DE");
const fs = require('fs');
const parse = require('csv-parse/lib/sync');
const commons = require('../replacements/commons');
const getJSON = require('get-json');
const Client = require('pg-native');
const client = new Client();
client.connectSync();
const arguments = process.argv.slice(2);

getJSON('https://www.coronavirus.sachsen.de/corona-statistics/rest/hospitalDevelopment.jsp', function(error, response){
 
  ['numberOfBeds', 'numberOfOccupiedBeds', 'numberOfItsBeds', 'numberOfOccupiedItsBeds'].forEach(key => {

    response[key].forEach(entry => {

      const date = moment(entry[0]);
      const value = entry[1];

      client.querySync(`
        INSERT INTO "measurement"("kreis_id","date","source","value","type","gemeinde_id")
        VALUES (14,'${date.format("YYYY-MM-DD")}'::date,'sms',${value},'${key}',0)
        ON CONFLICT(kreis_id, date, type, source, gemeinde_id) DO UPDATE SET value = EXCLUDED.value, delta_yesterday = EXCLUDED.delta_yesterday`);
    })
  })
});