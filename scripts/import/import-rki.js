var loader = require('csv-load-sync');
var helper = require('../../scripts/helper.js');
var sql = require('../../scripts/sql.js');
var Client = require('pg-native')
var client = new Client()
client.connectSync()

loader(`dist/AnzahlFall-rki-by-ags.csv`).forEach(function(csvrow){
  sumSaxony = 0;
  Object.keys(helper.agsToKreisIdMapping).forEach(function(ags){
    const kreis_id = helper.agsToKreisIdMapping[ags];
    const query = 
    `INSERT INTO public.measurement ("kreis_id","delta_yesterday","value","date","source","type","gemeinde_id") 
     VALUES (${kreis_id}, ${csvrow[ags]}, ${csvrow[ags]}, '${csvrow.date}', 'rki', 'infected', 0) 
     ON CONFLICT(kreis_id, date, type, source, gemeinde_id) DO UPDATE SET value = EXCLUDED.value, delta_yesterday = EXCLUDED.delta_yesterday
     RETURNING *`;
    sumSaxony+= parseInt(csvrow[ags]);
    try {
      
      client.querySync(query);
      
      if ( !sql.has7dIncidenceForDate(kreis_id, csvrow.date) ) {
        sql.insertKreisValue(kreis_id, "7d-incidence", sql.getXDaysIncidenceForDistrictOnDate(kreis_id, csvrow.date), csvrow.date );
        sql.insertKreisValue(kreis_id, "14d-incidence", sql.getXDaysIncidenceForDistrictOnDate(kreis_id, csvrow.date, 14), csvrow.date );
      }
    }
    catch (e) {
      console.log(`ERROR processing RKI infected file`)
      console.log(query, e)
      process.exit(1);
    }
  })
  // combine alle values for each day and ags to saxony value
  try {
    client.querySync(`INSERT INTO public.measurement ("kreis_id","delta_yesterday","value","date","source","type","gemeinde_id") 
    VALUES (14, ${sumSaxony}, ${sumSaxony}, '${csvrow.date}', 'rki', 'infected', 0) 
    ON CONFLICT(kreis_id, date, type, source, gemeinde_id) DO UPDATE SET value = EXCLUDED.value, delta_yesterday = EXCLUDED.delta_yesterday
    RETURNING *`);

    if ( !sql.has7dIncidenceForDate(14, csvrow.date) ) {
      sql.insertKreisValue(14, "7d-incidence", sql.getXDaysIncidenceForDistrictOnDate(14, csvrow.date), csvrow.date );
      sql.insertKreisValue(14, "14d-incidence", sql.getXDaysIncidenceForDistrictOnDate(14, csvrow.date, 14), csvrow.date );
    }
  }
  catch (e) {
    console.log(`ERROR processing RKI infected file`)
    console.log(query)
    process.exit(1);
  }
})

loader(`dist/AnzahlTodesfall-rki-by-ags.csv`).forEach(function(csvrow){
  sumSaxony = 0;
  Object.keys(helper.agsToKreisIdMapping).forEach(function(ags){
    const query  = 
    `INSERT INTO public.measurement ("kreis_id","delta_yesterday","value","date","source","type", "gemeinde_id") 
     VALUES (${helper.agsToKreisIdMapping[ags]}, ${csvrow[ags]}, ${csvrow[ags]}, '${csvrow.date}', 'rki', 'death', 0) 
     ON CONFLICT(kreis_id, date, type, source, gemeinde_id) DO UPDATE SET value = EXCLUDED.value, delta_yesterday = EXCLUDED.delta_yesterday
     RETURNING *`;
    sumSaxony+= parseInt(csvrow[ags]);
    try {
      client.querySync(query);
    }
    catch (e) {
      console.log(`ERROR processing RKI death file`)
      console.log(query)
      process.exit(1);
    }
  })
  // combine alle values for each day and ags to saxony value
  try {
    client.querySync(`
      INSERT INTO public.measurement ("kreis_id","delta_yesterday","value","date","source","type", "gemeinde_id") \
      VALUES (14, ${sumSaxony}, ${sumSaxony}, '${csvrow.date}', 'rki', 'death', 0) 
      ON CONFLICT(kreis_id, date, type, source, gemeinde_id) DO UPDATE SET value = EXCLUDED.value, delta_yesterday = EXCLUDED.delta_yesterday
      RETURNING *`);
  }
  catch (e) {
    console.log(`ERROR processing RKI infected file`)
    console.log(query)
    process.exit(1);
  }
})