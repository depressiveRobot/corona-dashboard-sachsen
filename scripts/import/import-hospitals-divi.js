const moment = require('moment');
moment.locale("de_DE");
const fs = require('fs');
const parse = require('csv-parse/lib/sync');
const commons = require('../replacements/commons');
const getJSON = require('get-json');
const Client = require('pg-native');
const client = new Client();
client.connectSync();
const arguments = process.argv.slice(2);

const processing = async function processing(date){
  
  const content = await fs.promises.readFile(`dist/DIVI-Intensivregister_${date}_12-15.csv`)
  parse(content, {columns : true}).filter(x => x.bundesland == '14').forEach(element => {
    
    Object.keys(commons.diviKeys).forEach(diviKey => {    
      
      if ( diviKey in element ) {
        const query =`
          INSERT INTO "measurement"("kreis_id","date","source","value","type","gemeinde_id")
          VALUES (${commons.agsToKreisIdMapping[element.gemeindeschluessel]},'${element.daten_stand}','DIVI',${element[diviKey]},'${commons.diviKeys[diviKey]}',0)
          ON CONFLICT(kreis_id, date, type, source, gemeinde_id) DO UPDATE SET value = EXCLUDED.value, delta_yesterday = EXCLUDED.delta_yesterday`;
      
        client.querySync(query);
      }
    })
  });
};

const date = arguments[0] || moment().subtract(1, 'days').format("YYYY-MM-DD");
const url  = `https://www.divi.de/joomlatools-files/docman-files/divi-intensivregister-tagesreports-csv/DIVI-Intensivregister_${date}_12-15.csv`;
commons.download(url, `./dist/DIVI-Intensivregister_${date}_12-15.csv`, function() {
  console.log(`Downloading of DIVI file ${url}`)
  processing(date);
});

getJSON('https://www.intensivregister.de/api/public/reporting/laendertabelle', function(error, response){
 
  const sachsen = response.data.filter(item => item.bundesland == 'SACHSEN')[0];
  for (const [key, value] of Object.entries(sachsen)) {
    
    if (key != 'creationTimestamp' && key != 'bundesland' && key != 'meldebereichAnz') {
      client.querySync(`
        INSERT INTO "measurement"("kreis_id","date","source","value","type","gemeinde_id")
        VALUES (14,'${response.creationTimestamp}'::date,'DIVI',${sachsen[key]},'${key}',0)
        ON CONFLICT(kreis_id, date, type, source, gemeinde_id) DO UPDATE SET value = EXCLUDED.value, delta_yesterday = EXCLUDED.delta_yesterday`);
    }
  }
});