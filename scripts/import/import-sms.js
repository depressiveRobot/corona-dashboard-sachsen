// Fetch a URL and parse all it's tables into JSON, using promises
var tabletojson = require('tabletojson').Tabletojson;
var url = 'https://www.coronavirus.sachsen.de/infektionsfaelle-in-sachsen-4151.html';
const jsdom = require("jsdom");
const moment = require('moment');
const fs = require('fs');
const path = require('path');
moment.locale("de_DE");
const { JSDOM } = jsdom;
// sync/blocking mode
var Client = require('pg-native')
var client = new Client()
client.connectSync()

const { program } = require('commander');

program
  .option('-d, --debug', 'output extra debugging')
  .option('-t, --tablename <type>', 'the name of the source database table name, used for debugging', 'measurement')
  .option('-f, --file <type>', 'an input file to process')
program.parse(process.argv);

if (program.debug) {
	console.log(program.opts());
}

var mappingKreise = {
	'Landeshauptstadt Dresden' : 1,
	'Stadt Chemnitz' : 2,
	'Stadt Leipzig' : 3,
	'Landkreis Bautzen' : 4,
	'Erzgebirgskreis' : 5,
	'Landkreis Görlitz' : 6,
	'Landkreis Leipzig' : 7,
	'Landkreis Meißen' : 8,
	'Landkreis Mittelsachsen' : 9,
	'Landkreis Nordsachsen' : 10,
	'Landkreis Sächsische Schweiz-Osterzgebirge' : 11,
	'Vogtlandkreis' : 12,
	'Landkreis Zwickau': 13,
	'Gesamtzahl': 14,
  'Gesamtzahl der Infektionen': 14,
  'Sachsen gesamt' : 14
} 

var parseData = function(tablesAsJson, dom) {

  // <p>Quelle: von der Landesuntersuchungsanstalt Sachsen übermittelte Fallzahlen |&nbsp;Stand: 9. April 2020, 13:00 Uhr</p>
  // <p>Genesene: Etwa 1.400 auf SARS CoV-2 positiv getestete Personen sind wieder genesen.&nbsp;Die Zahl der Genesenen ist ein Schätzwert.</p>
  var deaths, infected, recovered, date;
	
  dom.window.document.querySelectorAll("p").forEach(function(e){
    try {
      if (e.textContent.includes("Genesene: Etwa")) {
        recovered = e.textContent.replace("Genesene: Etwa ", "").replace(/\sauf\sSARS.*/g, "").replace(".", "");
      }
    }
    catch (e) {
      console.log(`Error getting number of recovered from file ${program.file}`);
    }

    // the date in the html is now wrong most of the time since it is not 
    // updated automatically, so we take the date from the crawl
    date = program.file.replace("coronavirus_", "").replace(".html", "");
    date = moment(date, "MM_DD_YYYY").format();

    // try {
    //   if (e.textContent.includes("Quelle: von der Landesuntersuchungsanstalt Sachsen übermittelte Fallzahlen")) {
    //     date = e.textContent.replace(/\s/g, " ").replace("Quelle: von der Landesuntersuchungsanstalt Sachsen übermittelte Fallzahlen |", '')
    //       .replace(" Stand: ", "").replace(" Uhr", " UTC+2").replace("Stand:", "")
    //       .replace(/\* Landesuntersuchungsanstalt Sachsen.*/g,"").trim();

    //     date = moment(date, "DD. MMMM YYYY, HH:mm z").format();
    //   }
    //   else if (e.textContent.startsWith('Stand: ') && date == undefined) {
    //     date = e.textContent.replace(/\s/g, " ").replace("Stand: ", '').replace(" Uhr", " UTC+2");

    //     date = moment(date, "DD. MMMM YYYY, HH:mm z").format();
    //   }
    //   // else {
    //   //   console.log(`Error getting date from file ${program.file}`);
    //   //   process.exit(1);  
    //   // }
    // }
    // catch (e) {
    //   console.log(`Error getting date from file ${program.file}`);
    //   process.exit(1);
    // }   
  });

  var tableIndex = 0;
  if (program.file && program.file.includes('coronavirus_03_15_2020.html')) tableIndex = 2; 

  console.log(date)

  // we only need the first table on the page
	tablesAsJson[tableIndex].forEach(function(row){

    // today the table was broken an the header is partially in the first row of the table
    if ( row["Kreisfreie Stadt / Landkreis"] != "Landkreis /\n                        kreisfreie Stadt" ) {

      var kreisId = mappingKreise[row['Kreisfreie Stadt / Landkreis']];
    
      try {

        // looks like this: "3.487 (+148)"
        var infectedKey = 'An Landesuntersuchungsanstalt\n                          elektronisch übermittelte Fälle\n                          (+ Vorabmeldungen)';
        if ( row.hasOwnProperty('An Landesuntersuchungsanstalt\n                          elektronisch übermittelte Fälle'))
          infectedKey = 'An Landesuntersuchungsanstalt\n                          elektronisch übermittelte Fälle'
        if ( row.hasOwnProperty('An LUA* elektronisch übermittelte Fälle – gesamt'))
          infectedKey = 'An LUA* elektronisch übermittelte Fälle – gesamt'
        if (row.hasOwnProperty('An Landesuntersuchungsanstalt\n                          elektronisch übermittelte Fälle (+ Differenz zum Vortrag)'))
          infectedKey = 'An Landesuntersuchungsanstalt\n                          elektronisch übermittelte Fälle (+ Differenz zum Vortrag)'
        infected = row[infectedKey]
        infected = infected.replace(/\./g, '').replace(/\(/g, ' ').replace(/ .*/g, '').trim();

        console.log(infected)
        console.log("")
        console.log("")
      }
      catch (e) {
        console.log(row);
        console.log(`Error getting number of infected from file ${program.file}`);
        process.exit(1);
      }

      try {
        // looks like this: '58 (+6) | Anteil 1,7%'
        var deathKey = 'Todesfälle in der Gesamtzahl der Fälle inbegriffen';
        if (row.hasOwnProperty('Todesfälle in der Gesamtzahl der Fälle inbegriffen (+ Differenz zum Vortrag)'))
          deathKey = 'Todesfälle in der Gesamtzahl der Fälle inbegriffen (+ Differenz zum Vortrag)'
        if (row.hasOwnProperty('Todesfälle** – gesamt'))
          deathKey = 'Todesfälle** – gesamt'

        deaths = row[deathKey];
        // in one row there is no space between the number and the bracket, so
        // first we remove the bracket and then replace from space to end
        deaths = deaths == undefined || deaths == '' ? 0 : deaths.replace(/\(/g, ' ').replace(/ .*/g, '').trim();
      }
      catch (e){
        console.log(`Error getting number of deaths from file ${program.file}`);
        process.exit(1);
      } 
    
      const infectedQuery  = `INSERT INTO "public"."${program.tablename}" ("kreis_id","value","date","source","type") VALUES (${kreisId}, ${infected}, \'${date}\', \'sms\', \'infected\') RETURNING *`;
      const deathsQuery    = `INSERT INTO "public"."${program.tablename}" ("kreis_id","value","date","source","type") VALUES (${kreisId}, ${deaths}, \'${date}\', \'sms\', \'death\') RETURNING *`;
      
      // the last row is the sum of all rows before
      if ( kreisId != 14 ) {
        try {
          client.querySync(infectedQuery);
          client.querySync(deathsQuery);
        }
        catch (e) {
          console.log(`ERROR processing file: ${program.file}`)
          console.log(infectedQuery)
          console.log(deathsQuery)
          console.log(e)
          process.exit(1);
        }
      }
      else {

        const infectedQuery  = `INSERT INTO "public"."${program.tablename}" ("kreis_id","value","date","source","type") VALUES (14, ${infected}, \'${date}\', \'sms\', \'infected\') RETURNING *`;
        const deathsQuery    = `INSERT INTO "public"."${program.tablename}" ("kreis_id","value","date","source","type") VALUES (14, ${deaths}, \'${date}\', \'sms\', \'death\') RETURNING *`
        const recoveredQuery = `INSERT INTO "public"."${program.tablename}" ("kreis_id","value","date","source","type") VALUES (14, ${recovered}, \'${date}\', \'sms\', \'recovered\') RETURNING *`

        try {
          client.querySync(infectedQuery);
          client.querySync(deathsQuery);
          if (recovered) {
            client.querySync(recoveredQuery);}
          else 
            if (moment(date).isAfter('2020-04-06'))
              console.log('no recoverd value found for table with date: ' + date)
        }
        catch (e) {
          console.log(`ERROR processing file: ${program.file}`)
          console.log(infectedQuery)
          console.log(deathsQuery)
          console.log(recoveredQuery)
          console.log(e)
          process.exit(1);
        }
      }
    }
	});
}

if ( program.file ) {
	const html = fs.readFileSync(path.resolve(__dirname, program.file), {encoding: 'UTF-8'});
  parseData(tabletojson.convert(html), new JSDOM(html));
}
else {
	tabletojson.convertUrl(url).then(function(tablesAsJson) {
    JSDOM.fromURL(url).then(dom => {
      parseData(tablesAsJson, dom);
    });
  })
}