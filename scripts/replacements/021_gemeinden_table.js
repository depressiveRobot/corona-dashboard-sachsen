const replace = require('replace-in-file');
let fs = require("fs");
const moment = require('moment');
const commons = require('./commons');
moment.locale("de_DE");
const sql = require('../../scripts/sql.js');
const helper = require('../../scripts/helper.js');

commons.kreise.forEach(function(kreis_id){

  let gemeinden = JSON.parse(fs.readFileSync('html/json/sachsen_gemeinden.geojson'));
  var rows = "";

  gemeinden.features.filter(gemeinde => gemeinde.properties.kreis_id == kreis_id).forEach(gemeinde => {
    rows += `
      <tr>
        <td><button type="button" class="btn btn-primary gemeinde-button" gemeinde-id="${gemeinde.properties.id}" kreis-id="${kreis_id}">Visualisierung</button></td>
        <td>${gemeinde.properties.gemeinde}</td>
        <td>${gemeinde.properties.einwohner}</td>
        <td>${sql.getValueForGemeindeById(gemeinde.properties.id, "7d-infections")[0]['value']}</td>
        <td>${sql.getValueForGemeindeById(gemeinde.properties.id, "7d-incidence")[0]['value']}</td>
      </tr>
    `
  });

  var table = `
    <table class="table" id="gemeinde-table-${kreis_id}">
      <thead>
        <tr>
          <th scope="col">Aktion</th>
          <th scope="col">Gemeinde</th>
          <th scope="col">Bevölkerung</th>
          <th scope="col">Fälle letzten 7 Tage</th>
          <th scope="col">7d-Inzidenz</th>
        </tr>
      </thead>
      <tbody>        
        ${rows}
      </tbody>
    </table>
  `;
  
  replace.sync({ files: 'dist/index_in_progress.html', from: `TABLE_${kreis_id}_GEMEINDEN_TABLE`, to: table });
})
