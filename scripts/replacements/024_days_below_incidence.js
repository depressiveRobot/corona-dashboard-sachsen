const replace = require('replace-in-file');
const fs = require("fs");
const path = require("path");
const moment = require('moment');
const _ = require('lodash');
const sql = require('../../scripts/sql.js');
const helper = require('../../scripts/helper.js');
const csv = require('../../scripts/csv.js');
const getJSON = require('get-json');
const commons = require('./commons');
const url = require('url');
const { countBy } = require('lodash');
const { diviKeys } = require('./commons');

var html = 
  `<div class="row mt-4">
    <div class="col-md">
    </div>
    <div class="col-md text-center">
      <h6>Inzidenz | Vgl. gestern</h6>
    </div>
    <div class="col-md text-center">
      <h6>Unter 200</h6>
    </div>
    <div class="col-md text-center">
      <h6>Unter 165</h6>
    </div>
    <div class="col-md text-center">
      <h6>Unter 100</h6>
    </div>
    <div class="col-md text-center" style="display: none;">
      <h6>Unter 75</h6>
    </div>
    <div class="col-md text-center"">
      <h6>Unter 50</h6>
    </div>
    <div class="col-md text-center">
      <h6>Unter 35</h6>
    </div>
  </div>`;

const getBackgroundColor = function(days){
  if ( days >= 5 ) return "#1a9641";
  if ( days >= 4 ) return "#a6d96a";
  if ( days >= 3 ) return "#ebfb8f";
  if ( days >= 2 ) return "#ffffc0";
  if ( days >= 1 ) return "#fdae61";
  if ( days >= 0 ) return "#d7191c";
}

const getPercentage = function(days) {
  if ( days >= 5 ) return 100;
  if ( days >= 4 ) return 80;
  if ( days >= 3 ) return 60;
  if ( days >= 2 ) return 40;
  if ( days >= 1 ) return 20;
  if ( days >= 0 ) return 0;
}

const getFontColor = function(days) {
  if ( days == 2 || days == 3) return "#000000";
  else return "#ffffff";
}

const getLabel = function(days) {
  if ( days == 1 ) return "1 Tag";
  else return days + " Tage";
}

commons.kreise.forEach(function(kreis_id){
  
  const counts = { 
    below_200 : 0,
    below_165 : 0,
    below_100 : 0,
    below_75 : 0,
    below_50 : 0,
    below_35 : 0
  };

  var htmlRows = "";
  // [200, 165, 100, 75, 50, 35]
  [200, 165, 100, 50, 35].forEach(incidenceThreshold => {
    // we walk back from the latest data to the first of the year
    _.range(0,100).every(function(index){
      const incidence = sql.get7dIncidenceForKreisToday(kreis_id, index)
      if ( sql.get7dIncidenceForKreisToday(kreis_id, index) < incidenceThreshold ) {
        counts["below_" + incidenceThreshold]++;
        return true;
      }
      else return false;
    });

    htmlRows += `
      <div class="col-md text-center">
        <div class="progress progress-red ${counts["below_" + incidenceThreshold] < 14 ? "below-14-days" : "above-14-days"}" style="height: 30px;">
          <span class="progress-value" style="color: ${getFontColor(counts["below_" + incidenceThreshold])}">${getLabel(counts["below_" + incidenceThreshold])}</span>
          <div class="progress-bar progress-bar bg-warning-${getPercentage(counts["below_" + incidenceThreshold])}" 
            role="progressbar" style="width: ${getPercentage(counts["below_" + incidenceThreshold])}%" aria-valuenow="${getPercentage(counts["below_" + incidenceThreshold])}" aria-valuemin="0" aria-valuemax="100">
          </div>
        </div>
      </div>`
  });

  const yesterdayIncidence = sql.get7dIncidenceForKreisYesterday(kreis_id);
  const todayIncidence     = sql.get7dIncidenceForKreisToday(kreis_id);
  const yesterdayDiff      = todayIncidence - yesterdayIncidence;

  html += `
    <div class="row mt-2">
      <div class="col-md text-right">
        ${commons.names[kreis_id].replace("Sächsische Schweiz-Osterzgebirge", "SSOE")
          .replace("Landkreis Leipzig", "LK Leipzig").replace("Sachsen", "<b>Sachsen</b>")
          .replace("Vogtlandkreis", "Vogtland").replace("Erzgebirgskreis", "Erzgebirge")}
      </div>
      <div class="col-md text-center">
      ${todayIncidence.toFixed(0)} | ${ (yesterdayDiff < 0 ? "" : "+" ) + yesterdayDiff.toFixed(1) } ${helper.getIncidenceTrendIcon(todayIncidence, yesterdayIncidence)}
      </div>
      ${htmlRows}
    </div>
  `;
})

html += `
<div class="row mt-4">
  <div class="col-md-2">
  </div>
  <div class="col-md text-center">
    <div class="progress progress-red below-14-days" style="height: 30px;">
      <span class="progress-value" style="color: #ffffff">Roter Rahmen bedeutet weniger als 14 Tage unter Grenzwert</span>
      <div class="progress-bar progress-bar bg-warning-100" role="progressbar" style="width: 100%" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100">
      </div>
    </div>
  </div>
  <div class="col-md text-center">
    <div class="progress progress-red above-14-days" style="height: 30px;">
      <span class="progress-value" style="color: #ffffff">Grüner Rahmen bedeutet länger als 14 Tage unter Grenzwert</span>
      <div class="progress-bar progress-bar bg-warning-100" role="progressbar" style="width: 100%" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100">
      </div>
    </div>
  </div>
</div>
`

replace.sync({ files: 'dist/index_in_progress.html', from: `DAYS_BELOW_INCIDENCE`, to: html });
