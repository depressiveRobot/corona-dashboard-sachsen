const replace = require('replace-in-file');
const fs = require("fs");
const path = require("path");
const moment = require('moment');
const _ = require('lodash');
const sql = require('../../scripts/sql.js');
const helper = require('../../scripts/helper.js');
const commons = require('./commons');
const csv = require('../../scripts/csv.js');


const numberOfDays = commons.days;

async function run(){

  var gemeindeData = {}
  sql.getGemeinden().forEach(gemeinde => {

    const result = {};
    sql.getMeasurementForGemeinde(gemeinde.id, '7d-incidence').forEach(measurement => {

      const date = moment(measurement.date).format('D.M.');
      if ( !(date in result) ) result[date] = [];
      result[date]['7d-incidence'] = measurement.value;
    });

    sql.getMeasurementForGemeinde(gemeinde.id, '7d-infections').forEach(measurement => {

      const date = moment(measurement.date).format('D.M.');
      if ( !(date in result) ) result[date] = [];
      result[date]['7d-infections'] = measurement.value;
    });

    // this is all a bit difficult, but like this we can enforce that the incidence and infections data
    // is from the same date, which is very important for the chart.js stuff
    gemeindeData[gemeinde.id] = {
      name  : gemeinde.gemeinde,
      dates : Object.keys(result),
      '7d-infections' : [],
      '7d-incidence'  : []
    }

    Object.keys(result).forEach(dateKey => {
      gemeindeData[gemeinde.id]['7d-infections'].push(result[dateKey]['7d-infections']);
      gemeindeData[gemeinde.id]['7d-incidence'].push(result[dateKey]['7d-incidence']);
    })
  })

  replace.sync({ files: 'dist/index_in_progress.html', from: `GEMEINDEN_JAVASCRIPT_NEEDLE`, to: `const gemeindenMeasurements = ${JSON.stringify(gemeindeData)};` });
}

run();
