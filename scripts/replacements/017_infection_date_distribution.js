// https://www.arcgis.com/sharing/rest/content/items/f10774f1c63e40168479a1feb6c7ca74/data 
const replace = require('replace-in-file');
const parse = require('csv-parse/lib/sync');
let fs = require("fs");
let path = require("path");
const _ = require('lodash');
const moment = require('moment');
var sql = require('../../scripts/sql.js');
const commons = require('./commons');

// https://www.arcgis.com/home/item.html?id=dd4580c810204019a7b8eb3e0b329dd6
function generateDateDistribution(newRecords, kreis_id) {

  // read
  const result = {};
  const filtered = newRecords
    .filter(row => (commons.rkiNamesMapping[row.Landkreis] == kreis_id && kreis_id != 14) || (true && kreis_id == 14))
    // .map(row => ({ date : row.Refdatum.replace(" 00:00:00", "").replace(/\//g, "-"), value: row.AnzahlFall }))
    .map(row => ({ date : row.Meldedatum.replace(" 00:00:00", "").replace(/\//g, "-"), value: row.AnzahlFall }))

  // merge
  filtered.forEach(row => { 
    if (result[row.date]) { 
      result[row.date] += Number(row.value); 
    } else { 
      result[row.date] = Number(row.value);
    }
  });

  // sort
  const ordered = {};
  Object.keys(result).sort().forEach(function(key) {
    ordered[key] = result[key];
  });

  return ordered;
}

async function runKreise(){

  const content = await fs.promises.readFile(`dist/RKI_COVID19.csv`)
  const newRecords = parse(content, {columns : true}).filter(item => item.NeuerFall == "1")

  commons.kreise.forEach(function(kreis_id){

    const newCases = generateDateDistribution(newRecords, kreis_id);
    var template = fs.readFileSync(path.join(__dirname, "../../html/date_distribution_template.js"), "utf8");

    template = template.replace(/DATE_DISTRIBUTION_LABELS/g, Object.keys(newCases).map(date => `"${moment(date).format("DD.MM.")}"`).join());
    template = template.replace(/DATE_DISTRIBUTION_LABEL/g, `Heutige neu berichtete Infektionen nach Meldedatum (RKI, Summe ${Object.values(newCases).reduce((a, b) => a + b, 0)})`);
    template = template.replace(/DATA_1/g, Object.values(newCases).join());
    template = template.replace(/DATA_LABEL_1/g, "Heutige neue Infektionen nach Meldedatum");
    fillTemplate(kreis_id, `infectionDateDistribution${kreis_id}`, template);
  });
}

function fillTemplate(kreis_id, canvasName, template){
  
  var onloadHtml = `
    var ctx = document.getElementById('${canvasName}Canvas').getContext('2d');
    window.canvas_age_incidence_${kreis_id} = new Chart(ctx, ${template});
    /* WINDOW_ONLOAD_NEEDLE */`;

  replace.sync({ files: 'dist/index_in_progress.html', from: `/* WINDOW_ONLOAD_NEEDLE */`, to: onloadHtml });
}

runKreise();

// IdBundesland: Id des Bundeslands des Falles mit 1=Schleswig-Holstein bis 16=Thüringen
// Bundesland: Name des Bundeslanes
// Landkreis ID: Id des Landkreises des Falles in der üblichen Kodierung 1001 bis 16077=LK Altenburger Land
// Landkreis: Name des Landkreises
// Altersgruppe: Altersgruppe des Falles aus den 6 Gruppe 0-4, 5-14, 15-34, 35-59, 60-79, 80+ sowie unbekannt
// Altersgruppe2: Altersgruppe des Falles aus 5-Jahresgruppen 0-4, 5-9, 10-14, ..., 75-79, 80+ sowie unbekannt
// Geschlecht: Geschlecht des Falles M=männlich, W=weiblich und unbekannt
// AnzahlFall: Anzahl der Fälle in der entsprechenden Gruppe
// AnzahlTodesfall: Anzahl der Todesfälle in der entsprechenden Gruppe
// Meldedatum: Datum, wann der Fall dem Gesundheitsamt bekannt geworden ist
// Datenstand: Datum, wann der Datensatz zuletzt aktualisiert worden ist
// NeuerFall: 

//     0: Fall ist in der Publikation für den aktuellen Tag und in der für den Vortag enthalten
//     1: Fall ist nur in der aktuellen Publikation enthalten
//     -1: Fall ist nur in der Publikation des Vortags enthalten
//     damit ergibt sich: Anzahl Fälle der aktuellen Publikation als Summe(AnzahlFall), wenn NeuerFall in (0,1); Delta zum Vortag als Summe(AnzahlFall) wenn NeuerFall in (-1,1)

// NeuerTodesfall:

//     0: Fall ist in der Publikation für den aktuellen Tag und in der für den Vortag jeweils ein Todesfall
//     1: Fall ist in der aktuellen Publikation ein Todesfall, nicht jedoch in der Publikation des Vortages
//     -1: Fall ist in der aktuellen Publikation kein Todesfall, jedoch war er in der Publikation des Vortags ein Todesfall
//     -9: Fall ist weder in der aktuellen Publikation noch in der des Vortages ein Todesfall
//     damit ergibt sich: Anzahl Todesfälle der aktuellen Publikation als Summe(AnzahlTodesfall) wenn NeuerTodesfall in (0,1); Delta zum Vortag als Summe(AnzahlTodesfall) wenn NeuerTodesfall in (-1,1)

// Referenzdatum: Erkrankungsdatum bzw. wenn das nicht bekannt ist, das Meldedatum
// AnzahlGenesen: Anzahl der Genesenen in der entsprechenden Gruppe
// NeuGenesen:

//     0: Fall ist in der Publikation für den aktuellen Tag und in der für den Vortag jeweils Genesen
//     1: Fall ist in der aktuellen Publikation Genesen, nicht jedoch in der Publikation des Vortages
//     -1: Fall ist in der aktuellen Publikation nicht Genesen, jedoch war er in der Publikation des Vortags Genesen
//     -9: Fall ist weder in der aktuellen Publikation noch in der des Vortages Genesen 
//     damit ergibt sich: Anzahl Genesen der aktuellen Publikation als Summe(AnzahlGenesen) wenn NeuGenesen in (0,1); Delta zum Vortag als Summe(AnzahlGenesen) wenn NeuGenesen in (-1,1)

// IstErkrankungsbeginn: 1, wenn das Refdatum der Erkrankungsbeginn ist, 0 sonst