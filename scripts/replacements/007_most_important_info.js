const replace = require('replace-in-file');
const moment = require('moment');
const commons = require('./commons');
moment.locale("de_DE");
const request = require('sync-request');
const sql = require('../../scripts/sql.js');
const helper = require('../../scripts/helper.js');

const numberWithCommas = (x) => {
  return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.');
}

// we reverse the order to avoid replace kreis 14 with with kreis 4 and "1"
commons.kreise.reverse().forEach(function(kreis_id){

  var html = `
            <div class="row mt-4">
              <div class="col-md-12">
                <h3 data-toc-text="Zusammenfassung">Die wichtigsten Entwicklungen, Datenstand: TABLE_DATE 00:00 Uhr</h3>
              </div>
            </div>
            <div class="row mt-4">

              <div class="col-md-6">
                <div class="card card-stats">
                  <div class="card-body">
                    <div class="row">
                      <div class="col">
                        <h5 class="card-title text-uppercase text-muted mb-0">Heutige Neuinfektionen</h5>
                        <span class="h2 font-weight-bold mb-0">
                          MOST_IMPORTANT_NEW_INFECTIONS
                      </div>
                      <div class="col-auto">
                        <div class="icon icon-shape bg-danger text-white rounded-circle shadow">
                          <i class="fas fa-signal"></i>
                        </div>
                      </div>
                    </div>
                    <!-- <p class="mt-3 mb-0 text-muted text-sm">
                      <span class="text-MOST_IMPORTANT_INCIDENCE_COLOR mr-2"><i class="fas MOST_IMPORTANT_INCIDENCE_ARROW"></i> MOST_IMPORTANT_INCIDENCE_RISE%</span>
                      <span class="text-nowrap">Since last week</span>
                    </p> -->
                  </div>
                </div>
              </div>
              <div class="col-md-6">
                <div class="card card-stats">
                  <div class="card-body">
                    <div class="row">
                      <div class="col">
                        <h5 class="card-title text-uppercase text-muted mb-0">Heutige Todesfälle</h5>
                        <span class="h2 font-weight-bold mb-0">
                          MOST_IMPORTANT_NEW_DEAD
                      </div>
                      <div class="col-auto">
                        <div class="icon icon-shape bg-black text-white rounded-circle shadow">
                          <i class="fas fa-cross"></i>
                        </div>
                      </div>
                    </div>
                    <!-- <p class="mt-3 mb-0 text-muted text-sm">
                      <span class="text-MOST_IMPORTANT_INCIDENCE_COLOR mr-2"><i class="fas MOST_IMPORTANT_INCIDENCE_ARROW"></i> MOST_IMPORTANT_INCIDENCE_RISE%</span>
                      <span class="text-nowrap">Since last week</span>
                    </p> -->
                  </div>
                </div>
              </div>
            </div>


            <div class="row mt-4">
              <div class="col-md-6">
                <div class="card card-stats">
                  <div class="card-body">
                    <div class="row">
                      <div class="col">
                        <h5 class="card-title text-uppercase text-muted mb-0">7-Tages-Neuinfektionen</h5>
                        <span class="h2 font-weight-bold mb-0">MOST_IMPORTANT_DAILY_NEW_7D_INFECTIONS</span>
                      </div>
                      <div class="col-auto">
                        <div class="icon icon-shape bg-danger text-white rounded-circle shadow">
                          <i class="fas fa-virus"></i>
                        </div>
                      </div>
                    </div>
                    <p class="mt-3 mb-0 text-muted text-sm">
                      <span class="text-MOST_IMPORTANT_DAILY_NEW_7D_INFECTIONS_COLOR mr-2"><i class="fa MOST_IMPORTANT_DAILY_NEW_7D_INFECTIONS_ARROW"></i> MOST_IMPORTANT_DAILY_NEW_7D_INFECTIONS_RISE%</span>
                      <span class="text-nowrap">im Vergleich zu gestern</span>
                    </p>
                    <p class="mb-0 text-muted text-sm">
                      <span class="text-MOST_IMPORTANT_DAILY_NEW_7D_INFECTIONS_COLOR_7D mr-2"><i class="fa MOST_IMPORTANT_DAILY_NEW_7D_INFECTIONS_ARROW_7D"></i> MOST_IMPORTANT_DAILY_NEW_7D_INFECTIONS_RISE_7D%</span>
                      <span class="text-nowrap">im Vergleich zur Vorwoche</span>
                    </p>
                  </div>
                </div>
              </div>

              <div class="col-md-6">
                <div class="card card-stats">
                  <div class="card-body">
                    <div class="row">
                      <div class="col">
                        <h5 class="card-title text-uppercase text-muted mb-0">7-Tages-Verstorbene</h5>
                        <span class="h2 font-weight-bold mb-0">MOST_IMPORTANT_DEATH_VALUE</span>
                      </div>
                      <div class="col-auto">
                        <div class="icon icon-shape bg-black text-white rounded-circle shadow">
                          <i class="fas fa-cross"></i>
                        </div>
                      </div>
                    </div>
                    <p class="mt-3 mb-0 text-muted text-sm">
                      <span class="text-MOST_IMPORTANT_DEATH_COLOR mr-2"><i class="fas MOST_IMPORTANT_DEATH_ARROW"></i> MOST_IMPORTANT_DEATH_RISE%</span>
                      <span class="text-nowrap">im Vergleich zu gestern</span>
                    </p>
                    <p class="mb-0 text-muted text-sm">
                      <span class="text-MOST_IMPORTANT_DEATH_COLOR_7D mr-2"><i class="fas MOST_IMPORTANT_DEATH_ARROW_7D"></i> MOST_IMPORTANT_DEATH_RISE_7D%</span>
                      <span class="text-nowrap">im Vergleich zur Vorwoche</span>
                    </p>
                  </div>
                </div>
              </div>
              
            </div>

            <div class="row mt-4">
              <div class="col-md-6">
                <div class="card card-stats">
                  <div class="card-body">
                    <div class="row">
                      <div class="col">
                        <h5 class="card-title text-uppercase text-muted mb-0">7-Tage-Inzidenz</h5>
                        <span class="h2 font-weight-bold mb-0">MOST_IMPORTANT_INCIDENCE_VALUE</span>
                      </div>
                      <div class="col-auto">
                        <div class="icon icon-shape bg-warning text-white rounded-circle shadow">
                          <i class="fas fa-users"></i>
                        </div>
                      </div>
                    </div>
                    <p class="mt-3 mb-0 text-muted text-sm">
                      <span class="text-MOST_IMPORTANT_INCIDENCE_COLOR mr-2"><i class="fas MOST_IMPORTANT_INCIDENCE_ARROW"></i> MOST_IMPORTANT_INCIDENCE_RISE%</span>
                      <span class="text-nowrap">im Vergleich zu gestern</span>
                    </p>
                    <p class="mb-0 text-muted text-sm">
                      <span class="text-MOST_IMPORTANT_INCIDENCE_COLOR_7D mr-2"><i class="fas MOST_IMPORTANT_INCIDENCE_ARROW_7D"></i> MOST_IMPORTANT_INCIDENCE_RISE_7D%</span>
                      <span class="text-nowrap">im Vergleich zur Vorwoche</span>
                    </p>
                  </div>
                </div>
              </div>

              <div class="col-md-6">
                <div class="card card-stats">
                  <div class="card-body">
                    <div class="row">
                      <div class="col">
                        <h5 class="card-title text-uppercase text-muted mb-0">R<sub class="text-lowercase">t</sub> - Reproduktionsfaktor </h5>
                        <span class="h2 font-weight-bold mb-0">
                          MOST_IMPORTANT_R_VALUE
                          <span class="supsub">
                              <span>MOST_IMPORTANT_R_VALUE_UP</span>
                              <span>MOST_IMPORTANT_R_VALUE_LOW</span>
                          </span>
                        </span>
                      </div>
                      <div class="col-auto">
                        <div class="icon icon-shape bg-warning text-white rounded-circle shadow">
                          <i class="fas fa-viruses"></i>
                        </div>
                      </div>
                    </div>
                    <p class="mt-3 mb-0 text-muted text-sm">
                      <!-- <span class="text-MOST_IMPORTANT_INCIDENCE_COLOR mr-2"><i class="fas MOST_IMPORTANT_INCIDENCE_ARROW"></i> MOST_IMPORTANT_INCIDENCE_RISE%</span> -->
                      <span class="text-nowrap">
                        <p>Möglich gemacht durch <a href="https://rtlive.de/">Open-Source-Software rt.live</a> vom <a href="https://fz-juelich.de/portal/DE/Home/home_node.html">Forschungszentrum Jülich</a>. Danke ❤️</p>
                      </span>
                    </p>
                  </div>
                </div>
              </div>
            </div>
  `;

  const today = moment(sql.getMaxDate());
  const last7DayInfectionsToday     = sql.getInfectionBetweenDates(kreis_id, moment(today), moment(sql.getMaxDate()).subtract(7, 'days'));
  const last7DayInfectionsLastWeek  = sql.getInfectionBetweenDates(kreis_id, moment(today).subtract(7, 'days'), moment(today).subtract(7 + 7, 'days'));
  const last7DayInfectionsYesterday = sql.getInfectionBetweenDates(kreis_id, moment(today).subtract(1, 'days'), moment(today).subtract(7 + 1, 'days'));
  const percentChangeInfections7D     = Number(-(100-((last7DayInfectionsToday*100)/last7DayInfectionsLastWeek))).toFixed(1);
  const percentChangeInfectionsYesterday = Number(-(100-((last7DayInfectionsToday*100)/last7DayInfectionsYesterday))).toFixed(1);

  html = html.replace(`GESUNDHEITSAMT_URL_${kreis_id}`, sql.getHealthOfficeUrl(kreis_id));
  html = html.replace(`TABLE_DATE`, moment(sql.getMaxDate()).format("DD.MM.YYYY"));

  html = html.replace(`MOST_IMPORTANT_DAILY_NEW_7D_INFECTIONS`, last7DayInfectionsToday);
  html = html.replace(`MOST_IMPORTANT_DAILY_NEW_7D_INFECTIONS_RISE_7D`, (percentChangeInfections7D >= 0 ? "+" : "") + percentChangeInfections7D);
  html = html.replace(`MOST_IMPORTANT_DAILY_NEW_7D_INFECTIONS_COLOR_7D`, percentChangeInfections7D <= 0 ? "success" : "danger");
  html = html.replace(`MOST_IMPORTANT_DAILY_NEW_7D_INFECTIONS_ARROW_7D`, percentChangeInfections7D <= 0 ? "fa-arrow-down" : "fa-arrow-up");
  html = html.replace(`MOST_IMPORTANT_DAILY_NEW_7D_INFECTIONS_RISE`, (percentChangeInfectionsYesterday >= 0 ? "+" : "") + percentChangeInfectionsYesterday);
  html = html.replace(`MOST_IMPORTANT_DAILY_NEW_7D_INFECTIONS_COLOR`, percentChangeInfectionsYesterday <= 0 ? "success" : "danger");
  html = html.replace(`MOST_IMPORTANT_DAILY_NEW_7D_INFECTIONS_ARROW`, percentChangeInfectionsYesterday <= 0 ? "fa-arrow-down" : "fa-arrow-up");
  
  const oneweekBeforeIncidence   = sql.get7dIncidenceForKreisOneWeekBefore(kreis_id);
  const yesterdayIncidence       = sql.get7dIncidenceForKreisYesterday(kreis_id);
  const todayIncidence           = sql.get7dIncidenceForKreisToday(kreis_id);
  const percentChangeIncidence   = Number(-(100-((todayIncidence*100)/yesterdayIncidence))).toFixed(1);
  const percentChangeIncidence7D = Number(-(100-((todayIncidence*100)/oneweekBeforeIncidence))).toFixed(1);

  html = html.replace(/MOST_IMPORTANT_INCIDENCE_VALUE/g, todayIncidence);
  html = html.replace(/MOST_IMPORTANT_INCIDENCE_RISE_7D/g, (percentChangeIncidence7D >= 0 ? "+" : "") + percentChangeIncidence7D);
  html = html.replace(/MOST_IMPORTANT_INCIDENCE_COLOR_7D/g, percentChangeIncidence7D <= 0 ? "success" : "danger");
  html = html.replace(/MOST_IMPORTANT_INCIDENCE_ARROW_7D/g, percentChangeIncidence7D <= 0 ? "fa-arrow-down" : "fa-arrow-up");
  html = html.replace(/MOST_IMPORTANT_INCIDENCE_RISE/g, (percentChangeIncidence >= 0 ? "+" : "") + percentChangeIncidence);
  html = html.replace(/MOST_IMPORTANT_INCIDENCE_COLOR/g, percentChangeIncidence <= 0 ? "success" : "danger");
  html = html.replace(/MOST_IMPORTANT_INCIDENCE_ARROW/g, percentChangeIncidence <= 0 ? "fa-arrow-down" : "fa-arrow-up");

  const todayDeath = sql.getInfectionBetweenDates(kreis_id, moment(today), moment(sql.getMaxDate()).subtract(7, 'days'), 'death-today');
  const lastWeekDeath = sql.getInfectionBetweenDates(kreis_id, moment(today).subtract(7, 'days'), moment(today).subtract(7 + 7, 'days'), 'death-today');
  const yesterdayDeath = sql.getInfectionBetweenDates(kreis_id, moment(today).subtract(1, 'days'), moment(today).subtract(7 + 1, 'days'), 'death-today');
  const percentChangeDeathLastWeek = Number(-(100-((todayDeath*100)/lastWeekDeath))).toFixed(1);
  const percentChangeDeathYesterday = Number(-(100-((todayDeath*100)/yesterdayDeath))).toFixed(1);

  html = html.replace(/MOST_IMPORTANT_DEATH_VALUE/g, todayDeath);
  html = html.replace(/MOST_IMPORTANT_DEATH_RISE_7D/g, (percentChangeDeathLastWeek >= 0 ? "+" : "") + percentChangeDeathLastWeek);
  html = html.replace(/MOST_IMPORTANT_DEATH_COLOR_7D/g, percentChangeDeathLastWeek < 0 ? "success" : "danger");
  html = html.replace(/MOST_IMPORTANT_DEATH_ARROW_7D/g, percentChangeDeathLastWeek < 0 ? "fa-arrow-down" : "fa-arrow-up");
  html = html.replace(/MOST_IMPORTANT_DEATH_RISE/g, (percentChangeDeathYesterday >= 0 ? "+" : "") + percentChangeDeathYesterday);
  html = html.replace(/MOST_IMPORTANT_DEATH_COLOR/g, percentChangeDeathYesterday < 0 ? "success" : "danger");
  html = html.replace(/MOST_IMPORTANT_DEATH_ARROW/g, percentChangeDeathYesterday < 0 ? "fa-arrow-down" : "fa-arrow-up");

  html = html.replace(/MOST_IMPORTANT_R_VALUE_UP/g, Number(sql.getRValue().r_t_upper).toFixed(2));
  html = html.replace(/MOST_IMPORTANT_R_VALUE_LOW/g, Number(sql.getRValue().r_t_lower).toFixed(2));
  html = html.replace(/MOST_IMPORTANT_R_VALUE/g, Number(sql.getRValue().r_t).toFixed(2));

  html = html.replace(/MOST_IMPORTANT_NEW_INFECTIONS/g, sql.getInfectionsForDistrictAndDate(kreis_id, moment(sql.getMaxDate()).format("YYYY-MM-DD"), "cases-today"));
  html = html.replace(/MOST_IMPORTANT_NEW_DEAD/g,       sql.getInfectionsForDistrictAndDate(kreis_id, moment(sql.getMaxDate()).format("YYYY-MM-DD"), "death-today"));

  if (kreis_id == 14 ) {

    const todayVaccinations = sql.getVaccinations(kreis_id, '(first|second)-vaccination-today$');
    const totalVaccinations = sql.getVaccinations(kreis_id, '(first|second)-vaccination-total$');

    html = html + `<div class="row mt-4">

              <div class="col-md-6">
                <div class="card card-stats">
                  <div class="card-body">
                    <div class="row">
                      <div class="col">
                        <h5 class="card-title text-uppercase text-muted mb-0">Heutige Impfungen</h5>
                        <span class="h2 font-weight-bold mb-0">
                          ${numberWithCommas(todayVaccinations[0].value + todayVaccinations[1].value)}
                      </div>
                      <div class="col-auto">
                        <div class="icon icon-shape bg-success text-white rounded-circle shadow">
                          <i class="fas fa-syringe"></i>
                        </div>
                      </div>
                    </div>
                    <p class="mt-3 mb-0 text-muted text-sm">
                      <!--span class="text-MOST_IMPORTANT_INCIDENCE_COLOR mr-2"><i class="fas MOST_IMPORTANT_INCIDENCE_ARROW"></i> MOST_IMPORTANT_INCIDENCE_RISE%</span-->
                      <span class="text-nowrap">${numberWithCommas(todayVaccinations[0].value)} Erstimpfungen & ${numberWithCommas(todayVaccinations[1].value)} Zweitimpfungen <br/>(Stand: ${moment(todayVaccinations[1].date).format("DD.MM.YYYY")})</span>
                    </p>
                  </div>
                </div>
              </div>
              <div class="col-md-6">
                <div class="card card-stats">
                  <div class="card-body">
                    <div class="row">
                      <div class="col">
                        <h5 class="card-title text-uppercase text-muted mb-0">Impfungen gesamt</h5>
                        <span class="h2 font-weight-bold mb-0">
                         ${numberWithCommas(totalVaccinations[0].value + totalVaccinations[1].value)}
                      </div>
                      <div class="col-auto">
                        <div class="icon icon-shape bg-success text-white rounded-circle shadow">
                          <i class="fas fa-syringe"></i>
                        </div>
                      </div>
                    </div>
                    <p class="mt-3 mb-0 text-muted text-sm">
                      <!--span class="text-MOST_IMPORTANT_INCIDENCE_COLOR mr-2"><i class="fas MOST_IMPORTANT_INCIDENCE_ARROW"></i> MOST_IMPORTANT_INCIDENCE_RISE%</span-->
                      <span class="text-nowrap">${numberWithCommas(totalVaccinations[0].value)} Erstimpfungen & ${numberWithCommas(totalVaccinations[1].value)} Zweitimpfungen <br/>(Stand: ${moment(totalVaccinations[1].date).format("DD.MM.YYYY")})</span>
                    </p>
                  </div>
                </div>
              </div>
            </div>`
  }
  
  replace.sync({ files: 'dist/index_in_progress.html', from: `${kreis_id}_MOST_IMPORTANT_INFO`, to: html });
})
