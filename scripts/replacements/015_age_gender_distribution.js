// https://www.arcgis.com/sharing/rest/content/items/f10774f1c63e40168479a1feb6c7ca74/data 
const replace = require('replace-in-file');
let fs = require("fs");
let path = require("path");
const _ = require('lodash');
const moment = require('moment');
var sql = require('../../scripts/sql.js');
var csv = require('../../scripts/csv.js');
const commons = require('./commons');
const ageGroups = ['A00-A04', 'A05-A14', 'A15-A34', 'A35-A59', 'A60-A79', 'A80+'];

async function runKreise(){

  const rows = await csv.getRkiRows();
  
  commons.kreise.forEach(function(kreis_id){

    var male = [];
    ageGroups.forEach(ageGroup => {
      male.push(csv.getDistributionInAgeGroup(rows, ageGroup, "M", kreis_id))
    });
    var female = [];
    ageGroups.forEach(ageGroup => {
      female.push(csv.getDistributionInAgeGroup(rows, ageGroup, "W", kreis_id))
    });

    var infectionsTemplate = fs.readFileSync(path.join(__dirname, "../../html/age_gender_infections_template.js"), "utf8");
    infectionsTemplate = infectionsTemplate.replace(/ageGenderKREIS_IDConfig/g, `ageGender${kreis_id}Config`);
    infectionsTemplate = infectionsTemplate.replace(/AGE_GROUP_GENDER_MALE/g, male.join());
    infectionsTemplate = infectionsTemplate.replace(/AGE_GROUP_GENDER_FEMALE/g, female.join());
    infectionsTemplate = infectionsTemplate.replace(/AGE_GROUP_GENDER_LABEL/g, "Infektionen, gesamt (RKI)");
    infectionsTemplate = infectionsTemplate.replace('IMPORTANT_DATES', `${JSON.stringify(commons.importantDates).replace(/^\[/g, "").replace(/\]$/g, "")}`);
    fillTemplate(kreis_id, `ageGender${kreis_id}`, infectionsTemplate);

    var male = [];
    ageGroups.forEach(ageGroup => {
      male.push(csv.getDistributionInAgeGroup(rows, ageGroup, "M", kreis_id, 7))
    });
    var female = [];
    ageGroups.forEach(ageGroup => {
      female.push(csv.getDistributionInAgeGroup(rows, ageGroup, "W", kreis_id, 7))
    });

    var infectionsTemplate = fs.readFileSync(path.join(__dirname, "../../html/age_gender_infections_template.js"), "utf8");
    infectionsTemplate = infectionsTemplate.replace(/ageGenderKREIS_IDConfig/g, "ageGenderLastSevenDays14Config");
    infectionsTemplate = infectionsTemplate.replace(/AGE_GROUP_GENDER_MALE/g, male.join());
    infectionsTemplate = infectionsTemplate.replace(/AGE_GROUP_GENDER_FEMALE/g, female.join());
    infectionsTemplate = infectionsTemplate.replace(/AGE_GROUP_GENDER_LABEL/g, "Infektionen, letzten 7 Tage (RKI)");
    infectionsTemplate = infectionsTemplate.replace('IMPORTANT_DATES', `${JSON.stringify(commons.importantDates).replace(/^\[/g, "").replace(/\]$/g, "")}`);
    fillTemplate(kreis_id, `ageGenderLastSevenDays${kreis_id}`, infectionsTemplate);
  });
}

function fillTemplate(kreis_id, canvasName, template){
  
  var onloadHtml = `
    var ctx = document.getElementById('${canvasName}Canvas').getContext('2d');
    window.canvas_age_incidence_${kreis_id} = new Chart(ctx, ${template});
    /* WINDOW_ONLOAD_NEEDLE */`;

  replace.sync({ files: 'dist/index_in_progress.html', from: `/* WINDOW_ONLOAD_NEEDLE */`, to: onloadHtml });
}

runKreise();