const replace = require('replace-in-file');
const fs = require("fs");
const path = require("path");
const moment = require('moment');
const _ = require('lodash');
const sql = require('../../scripts/sql.js');
const helper = require('../../scripts/helper.js');
const csv = require('../../scripts/csv.js');
const commons = require('./commons');
const url = require('url');
const { countBy } = require('lodash');

const mapping = {
  Vogtlandkreis: 12,
  Dresden: 1,
  Chemnitz: 2,
  Leipzig: 3,
  Bautzen: 4,
  Meißen: 8,
  'Landkreis Leipzig' : 7,
  'LK Görlitz': 6,
  Mittelsachsen: 9,
  Erzgebirgskreis: 5,
  Zwickau: 13,
  'Sächsische Schweiz-Osterzgebirge': 11,
  'SSOE': 11,
  'Görlitz': 6,
  Nordsachsen: 10
}
let total = 0;
const counts = { 1:0, 2:0, 3:0, 4:0, 5:0, 6:0, 7:0, 8:0, 9:10, 10:0, 11:0, 12:0, 13:0 }

async function run(){

  const rows = (await csv.getRows('dist/covid-mutanten.csv')).filter(item => item.Bundesland == 'Sachsen');
  let rowsHtml = "";
  let maxDate = moment("2020-01-01");

  // group all the values to the districts
  rows.forEach(row => {
    
    var id = '';
    for (const [key, value] of Object.entries(commons.names) ) { id = mapping[row.Landkreis]; }
    if ( id == '' ) console.log("Error, coult not match the line", row)
    else {
      counts[id] += Number(row['Sichere Fälle']);
      total += Number(row['Sichere Fälle']); // sum all for total count 

      rowsHtml += `<tr>
        <td>${commons.names[id]}</td>
        <td>${row['Sichere Fälle']}</td>
        <td>${row.Variante}</td>
        <td>${moment(row.Meldungsdatum).format("DD.MM.YY")}</td>
        <td><a target="_blank" href="${row.Quelle}">${url.parse(row.Quelle).host.replace("www.", "")}</a></td>
      </tr>`

      maxDate = moment.max(maxDate, moment(row.Meldungsdatum));
    }
  });

  // write the cases to the file
  let sachsen = JSON.parse(fs.readFileSync('dist/json/sachsen.js','utf-8').replace("var sachsen = ", ""));
  sachsen.features.forEach(kreis => {
    kreis.properties['mutants'] = counts[kreis.properties.id.toString()];
  });
  fs.writeFile('dist/json/sachsen.js', `var sachsen = ${JSON.stringify(sachsen)}`, (err) => {
    if (err) throw err;
  });

  // write the mutants data to a table
  const table = `<table class="table table-striped" id="mutants-table">
    <thead>
      <tr>
        <th scope="col">Landkreis</th>
        <th scope="col">Fälle</th>
        <th scope="col">Variante</th>
        <th scope="col">Meldedatum</th>
        <th scope="col">Quelle</th>
      </tr>
    </thead>
    <tbody>
      ${rowsHtml}
    </tbody>
  </table>`

  const maxValue = Math.max(...Object.values(counts));
  replace.sync({ files: 'dist/index_in_progress.html', from: `MUTANTS_MAX_VALUE_PER_DISTRICT`, to: maxValue });
  replace.sync({ files: 'dist/index_in_progress.html', from: `MUTANTS_TABLE`, to: table });
  replace.sync({ files: 'dist/index_in_progress.html', from: `NEWEST_MUTANT_DATE`, to: maxDate.format("DD.MM.YYYY") });
}

run();

// replace.sync({ files: 'dist/index_in_progress.html', from: `INCIDENCE_DIFF_DATASETS`, to: results.values });
// replace.sync({ files: 'dist/index_in_progress.html', from: `INCIDENCE_DIFF_LABELS`,   to: results[12].labels.reverse().toString() });
