const fs = require("fs");
const path = require("path");
const https = require('https');

const download = function(url, dest, cb) {
  var file = fs.createWriteStream(dest);
  var request = https.get(url, function(response) {
    response.pipe(file);
    file.on('finish', function() {
      file.close(cb);  // close() is async, call cb after close completes.
    });
  }).on('error', function(err) { // Handle errors
    fs.unlink(dest); // Delete the file async. (But we don't check the result)
    if (cb) cb(err.message);
  });
};

module.exports = {
  download : download,
  // kreise : [12],
  kreise : [1,2,3,4,5,6,7,8,9,10,11,12,13,14],
  sourceForInfections: 'rki',
  days : 100,
  ageGroups: ['A00-A04', 'A05-A14', 'A15-A34', 'A35-A59', 'A60-A79', 'A80+'],
  rkiNamesMapping: {
    'SK Dresden' : '1',
    'SK Chemnitz' : '2',
    'SK Leipzig' : '3',
    'LK Bautzen' : '4',
    'LK Erzgebirgskreis' : '5',
    'LK Görlitz' : '6',
    'LK Leipzig' : '7',
    'LK Meißen' : '8',
    'LK Mittelsachsen' : '9',
    'LK Nordsachsen' : '10',
    'LK Sächsische Schweiz-Osterzgebirge' : '11',
    'LK Vogtlandkreis' : '12',
    'LK Zwickau' : '13',
    'Sachsen' : '14'
  },
  slugs : {
    '1' : 'dresden',
    '2' : 'chemnitz',
    '3' : 'leipzig',
    '4' : 'bautzen',
    '5' : 'erzgebirge',
    '6' : 'goerlitz',
    '7' : 'landkreis-leipzig',
    '8' : 'meissen',
    '9' : 'mittelsachsen',
    '10' : 'nordsachsen',
    '11' : 'saechsische-schweiz-osterzgebirge',
    '12' : 'vogtlandkreis',
    '13' : 'zwickau',
    '14' : 'sachsen'
  },
  names : {
    '1' : 'Dresden',
    '2' : 'Chemnitz',
    '3' : 'Leipzig',
    '4' : 'Bautzen',
    '5' : 'Erzgebirgskreis',
    '6' : 'Görlitz',
    '7' : 'Landkreis Leipzig',
    '8' : 'Meißen',
    '9' : 'Mittelsachsen',
    '10' : 'Nordsachsen',
    '11' : 'Sächsische Schweiz-Osterzgebirge',
    '12' : 'Vogtlandkreis',
    '13' : 'Zwickau',
    '14' : 'Sachsen'
  },
  population : {
    '1'  : 551072,
    '2'  : 246855,
    '3'  : 581980,
    '4'  : 302634,
    '5'  : 340373,
    '6'  : 256587,
    '7'  : 258008,
    '8'  : 242862,
    '9'  : 308153,
    '10' : 197794,
    '11' : 245418,
    '12' : 229584,
    '13' : 319998,
    '14' : 4077937
  },
  agsToKreisIdMapping : {
    "14612": 1,
    "14511": 2,
    "14713": 3,
    "14625": 4,
    "14521": 5,
    "14626": 6,
    "14729": 7,
    "14627": 8,
    "14522": 9,
    "14730": 10,
    "14628": 11,
    "14523": 12,
    "14524": 13
  },
  kreisIdToAgsMapping : {
    1 : "14612",
    2 : "14511",
    3 : "14713",
    4 : "14625",
    5 : "14521",
    6 : "14626",
    7 : "14729",
    8 : "14627",
    9 : "14522",
    10 : "14730",
    11 : "14628",
    12 : "14523",
    13 : "14524"
  },
  colors : {
    '1'  : '#EF767A',
    '2'  : '#456990',
    '3'  : '#49BEAA',
    '4'  : '#49DCB1',
    '5'  : '#EEB868',
    '6'  : '#DB5ABA',
    '7'  : '#CF8BA3',
    '8'  : '#E5CDC8',
    '9'  : '#0B3954',
    '10' : '#BFD7EA',
    '11' : '#FF6663',
    '12' : '#E0FF4F',
    '13' : '#9FFFF5',
    '14' : '#0E34A0'
  },
  diviKeys : {
    anzahl_meldebereiche: 'anzahl_meldebereiche',
    faelle_covid_aktuell: 'faelleCovidAktuell',
    faelle_covid_aktuell_beatmet: 'faelleCovidAktuellBeatmet',
    faelle_covid_aktuell_invasiv_beatmet: 'faelleCovidAktuellBeatmet',
    anzahl_standorte: 'anzahl_standorte',
    betten_frei: 'intensivBettenFrei',
    betten_belegt: 'intensivBettenBelegt',
  },
  importantDates : [
    {
      type: "line",
      mode: "vertical",
      scaleID: "x-axis-0",
      value: "07.11.",
      borderColor: "rgb(75, 192, 192)",
      borderWidth: 4,
      label: {
        content: "#LE0711",
        enabled: true,
        position: "top",
        rotation: 0
      }
    },
    {
      type: "line",
      mode: "vertical",
      scaleID: "x-axis-0",
      value: "13.11.",
      borderColor: "rgb(75, 192, 192)",
      borderWidth: 4,
      label: {
        content: "SächsCoronaSchVO",
        enabled: true,
        position: "top",
        rotation: 0
      }
    },
    {
      type: "line",
      mode: "vertical",
      scaleID: "x-axis-0",
      value: "01.11.",
      borderColor: "rgb(75, 192, 192)",
      borderWidth: 4,
      label: {
        content: "Gastro/Kultur geschlossen",
        enabled: true,
        position: "top",
        rotation: 0
      }
    },
    {
      type: "line",
      mode: "vertical",
      scaleID: "x-axis-0",
      value: "01.12.",
      borderColor: "rgb(75, 192, 192)",
      borderWidth: 4,
      label: {
        content: "Kontaktbeschränkung",
        enabled: true,
        position: "top",
        rotation: 0
      }
    },
    {
      type: "line",
      mode: "vertical",
      scaleID: "x-axis-0",
      value: "14.12.",
      borderColor: "rgb(75, 192, 192)",
      borderWidth: 4,
      label: {
        content: "Lockdown2",
        enabled: true,
        position: "top",
        rotation: 0
      }
    },
    {
      type: "line",
      mode: "vertical",
      scaleID: "x-axis-0",
      value: "24.12.",
      borderColor: "rgb(75, 192, 192)",
      borderWidth: 4,
      label: {
        content: "Weihnachten",
        enabled: true,
        position: "top",
        rotation: 0
      }
    },
    {
      type: "line",
      mode: "vertical",
      scaleID: "x-axis-0",
      value: "31.12.",
      borderColor: "rgb(75, 192, 192)",
      borderWidth: 4,
      label: {
        content: "Silvester",
        enabled: true,
        position: "top",
        rotation: 0
      }
    },
    {
      type: "line",
      mode: "vertical",
      scaleID: "x-axis-0",
      value: "18.01.",
      borderColor: "rgb(75, 192, 192)",
      borderWidth: 4,
      label: {
        content: "Abschlussklassen",
        enabled: true,
        position: "top",
        rotation: 0
      }
    },
    {
      type: "line",
      mode: "vertical",
      scaleID: "x-axis-0",
      value: "15.02.",
      borderColor: "rgb(75, 192, 192)",
      borderWidth: 4,
      label: {
        content: "Grundschulen und Kitas",
        enabled: true,
        position: "top",
        rotation: 0
      }
    },
    {
      type: "line",
      mode: "vertical",
      scaleID: "x-axis-0",
      value: "08.03.",
      borderColor: "rgb(75, 192, 192)",
      borderWidth: 4,
      label: {
        content: "Zwei Haushalte",
        enabled: true,
        position: "bottom",
        rotation: 0
      }
    },
    {
      type: "line",
      mode: "vertical",
      scaleID: "x-axis-0",
      value: "15.03.",
      borderColor: "rgb(75, 192, 192)",
      borderWidth: 4,
      label: {
        content: "Wechselmodell",
        enabled: true,
        position: "top",
        rotation: 0
      }
    },
    {
      type: "line",
      mode: "vertical",
      scaleID: "x-axis-0",
      value: "02.04.",
      borderColor: "rgb(75, 192, 192)",
      borderWidth: 4,
      label: {
        content: "Ostern",
        enabled: true,
        position: "top",
        rotation: 0
      }
    },
    {
      type: "line",
      mode: "vertical",
      scaleID: "x-axis-0",
      value: "27.03.",
      borderColor: "rgb(75, 192, 192)",
      borderWidth: 4,
      label: {
        content: "Osterferien",
        enabled: true,
        position: "bottom",
        rotation: 90
      }
    },
    {
      type: "line",
      mode: "vertical",
      scaleID: "x-axis-0",
      value: "23.04.",
      borderColor: "rgb(75, 192, 192)",
      borderWidth: 4,
      label: {
        content: "IfSG",
        enabled: true,
        position: "bottom",
        rotation: 90
      }
    }
  ]
};