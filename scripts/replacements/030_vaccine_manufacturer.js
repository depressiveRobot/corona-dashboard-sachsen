const replace = require('replace-in-file');
const moment = require('moment');
const _ = require('lodash');
const sql = require('../../scripts/sql.js');
const helper = require('../../scripts/helper.js');
const commons = require('./commons');

const keyToManufacterer = (x) => {
  x = x.replace("delivery-", "");
  return (x == "comirnaty" ? "Pfizer/Biontech" : (x == "astra" ? "AstraZeneca" : (x == "moderna" ? "Moderna" : "unbekannt")));
}

async function run(){

  const result = sql.getVaccinationManufacturers();

  var onloadHtml = `
  var ctx = document.getElementById('vaccinationOriginCanvas').getContext('2d');
  window.vaccinationOriginCanvas = new Chart(ctx, {
      type: 'doughnut',
      data: {
        labels: ['${keyToManufacterer(result[0].type)}','${keyToManufacterer(result[1].type)}','${keyToManufacterer(result[2].type)}'],
        datasets: [{
          label: 'Hersteller der Impfstoffe',
          backgroundColor: [window.chartColors.red, window.chartColors.orange, window.chartColors.yellow],
          data: [${result[0].sum},${result[1].sum},${result[2].sum}],
          fill: false,
          hidden: false
        }]
      },
      options: {
        cutout : '20%',
        responsive: true,
        title: { display: false },
        tooltips: { mode: 'index', intersect: false },
        hover: { mode: 'nearest', intersect: true },
        legend: { position: 'bottom' }
      }
    });

    /* WINDOW_ONLOAD_NEEDLE */`
  replace.sync({ files: 'dist/index_in_progress.html', from: `/* WINDOW_ONLOAD_NEEDLE */`, to: onloadHtml });

  const totalVaccinations = sql.getVaccinations(14, '(first|second)-vaccination-total$');

  replace.sync({ files: 'dist/index_in_progress.html', from: `SUM_VACCINATIONS_DELIVERED`, to: result[0].sum + result[1].sum + result[2].sum });
  replace.sync({ files: 'dist/index_in_progress.html', from: `SUM_VACCINATIONS_FREE`, to: (result[0].sum + result[1].sum + result[2].sum) - (totalVaccinations[0].value + totalVaccinations[1].value) });
}

run();
