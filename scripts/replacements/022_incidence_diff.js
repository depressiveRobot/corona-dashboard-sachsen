const replace = require('replace-in-file');
const fs = require("fs");
const path = require("path");
const moment = require('moment');
const _ = require('lodash');
const sql = require('../../scripts/sql.js');
const helper = require('../../scripts/helper.js');
const commons = require('./commons');

const results      = { 'values' : ''};
const numberOfDays = commons.days;

commons.kreise.forEach(kreis_id => {

  results[kreis_id] = {};
  results[kreis_id].values = [];
  results[kreis_id].labels = [];
  
  for (let index = 0; index < numberOfDays; index++) {
      
    const yesterdayIncidence = sql.get7dIncidenceForKreisToday(kreis_id, index + 1);
    const todayIncidence     = sql.get7dIncidenceForKreisToday(kreis_id, index);
  
    results[kreis_id].values.push(_.round(todayIncidence - yesterdayIncidence, 1));
    results[kreis_id].labels.push("\"" + moment(sql.getMaxDate()).subtract(index, 'days').format("D.M.") + "\"")
  }

  results.values += `{
    label: '${helper.getName(kreis_id)}',
    backgroundColor: '${helper.getColor(kreis_id)}',
    borderColor: '${helper.getColor(kreis_id)}',
    fill: 'origin',
    data: [${results[kreis_id].values.reverse().toString()}],
  },`
});

replace.sync({ files: 'dist/index_in_progress.html', from: `INCIDENCE_DIFF_DATASETS`, to: results.values });
replace.sync({ files: 'dist/index_in_progress.html', from: `INCIDENCE_DIFF_LABELS`,   to: results[12].labels.reverse().toString() });
