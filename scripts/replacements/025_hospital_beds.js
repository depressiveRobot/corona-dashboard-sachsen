const replace = require('replace-in-file');
const fs = require("fs");
const path = require("path");
const moment = require('moment');
const _ = require('lodash');
const sql = require('../../scripts/sql.js');
const helper = require('../../scripts/helper.js');
const csv = require('../../scripts/csv.js');
const getJSON = require('get-json');
const commons = require('./commons');
const url = require('url');
const { countBy } = require('lodash');
const { diviKeys } = require('./commons');

getJSON("https://www.coronavirus.sachsen.de/corona-statistics/rest/hospitalDevelopment.jsp", function(error, response){
  
  let html = "";

  _.range(5).forEach(index => {

    const numberOfOccupiedBeds    = response.numberOfOccupiedBeds[response.numberOfOccupiedBeds.length - 1 - index];
    const numberOfOccupiedItsBeds = response.numberOfOccupiedItsBeds[response.numberOfOccupiedItsBeds.length - 1 - index];
    const date = moment(numberOfOccupiedBeds[0]).add(1, 'days').format("DD.MM.YYYY");
    const percentage = Number(((numberOfOccupiedBeds[1] - numberOfOccupiedItsBeds[1]) / 1300.0) * 100).toFixed(1);

    html += `
    <div class="row mt-2">
      <div class="col-md-2 text-center">
        ${date}
      </div>
      <div class="col-md-10 text-center">
        <div class="progress-bar-beds-${index} progress-bar-beds" data-amount="${percentage}">
          <div class="amount"></div>
        </div>
      </div>
    </div>
    `
  });

  replace.sync({ files: 'dist/index_in_progress.html', from: `HOSPITAL_BEDS_OCCUPANCY`, to: html });
});