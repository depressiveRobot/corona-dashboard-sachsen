// https://www.arcgis.com/sharing/rest/content/items/f10774f1c63e40168479a1feb6c7ca74/data 
const replace = require('replace-in-file');
const fs = require('fs');
const _ = require('lodash');
const moment = require('moment');
var sql = require('../../scripts/sql.js');
const commons = require('./commons');

async function run(){
  
  const deaths = sql.getTotalDeaths();
   
  replace.sync({ files: 'dist/index_in_progress.html', from: `DEATH_CIRCLE_LABELS`, 
    to: Object.values(deaths).map(key => `"${commons.names[key]}"`).join()});
  replace.sync({ files: 'dist/index_in_progress.html', from: `DEATH_CIRCLE_COLORS`, 
    to: Object.values(deaths).map(key => `"${commons.colors[key]}"`).join()});
  replace.sync({ files: 'dist/index_in_progress.html', from: `DEATH_CIRCLE_VALUES`, 
    to: Object.keys(deaths).join() });

  const infections = sql.getTotalInfections();

  replace.sync({ files: 'dist/index_in_progress.html', from: `INFECTION_CIRCLE_LABELS`, 
    to: Object.values(infections).map(key => `"${commons.names[key]}"`).join()});
  replace.sync({ files: 'dist/index_in_progress.html', from: `INFECTION_CIRCLE_COLORS`, 
    to: Object.values(infections).map(key => `"${commons.colors[key]}"`).join()});
  replace.sync({ files: 'dist/index_in_progress.html', from: `INFECTION_CIRCLE_VALUES`, 
    to: Object.keys(infections).join() });
}

run();