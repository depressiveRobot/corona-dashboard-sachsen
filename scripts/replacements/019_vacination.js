// https://www.arcgis.com/sharing/rest/content/items/f10774f1c63e40168479a1feb6c7ca74/data 
const replace = require('replace-in-file');
let fs = require("fs");
let path = require("path");
const _ = require('lodash');
const moment = require('moment');
var csv = require('../../scripts/csv.js');
const commons = require('./commons');

async function doVacintations(){

  const rows = await csv.getVacinationRows(path.join(__dirname, "../../data/rki/impfquotenmonitoring_sachsen.csv"));
  const dailyDiffs = rows.map(row => Number(row["Differenz zum Vortag"]));
  const start = moment("2020-12-27");
  const end = moment(rows[rows.length - 1].Datenstand, "DD.MM.YYYY")
  const daysSinceStart = end.diff(start, 'days');
  
  var template = fs.readFileSync(path.join(__dirname, "../../html/daily_vacination_template.js"), "utf8");
  template = template.replace(/GRAPH_TITLE/g, "Impfungen pro Tag");
  template = template.replace(/Y-AXIS_LABEL/g, "Anzahl");
  template = template.replace(/X-AXIS_LABEL/g, "Datum");
  template = template.replace(/LABELS/g, rows.map(row => `"${moment(row.Datenstand, "DD.MM.YYYY").format("DD.MM.")}"`).join());
  template = template.replace(/DATA_TOTAL/g, dailyDiffs.join());
  template = template.replace(/LABEL_TOTAL/g, "Differenz zum Vortag");

  fillTemplate(14, `vacinationValues14`, template);

  const averageVacinationsPerday = rows[rows.length - 1]["Impfungen kumulativ"] / daysSinceStart;

  var table = `
    <table class="table table-striped">
      <thead>
        <tr>
          <th scope="col" colspan="2">Gesamtübersicht <small>(Stand: ${rows[rows.length - 1]["Datenstand"]})</small</th>
          <th scope="col"></th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td>Tage seit Impfstart</td>
          <td>${daysSinceStart}</td>
        </tr>
        <tr>
          <td>Ø an Impfungen/Tag</td>
          <td>${averageVacinationsPerday.toFixed()}</td>
        </tr>
        <tr>
          <td>Impfungen pro 1.000 Einwohnende</td>
          <td>${Number(rows[rows.length - 1]["Impfungen pro 1.000 Einwohner"]).toFixed(1)}</td>
        </tr>
        <tr>
          <td>Impfungen gesamt</td>
          <td>${rows[rows.length - 1]["Impfungen kumulativ"]}</td>
        </tr>
        <tr>
          <td>Tage bis Herdenimmunität</td>
          <td>${((4071971 * 0.7) / averageVacinationsPerday).toFixed()} (${(((4071971 * 0.7)/averageVacinationsPerday).toFixed() / 365).toFixed(1)} Jahre)</td>
        </tr>
        <tr>
          <td>Ø Pflegeheimbewohner*in/Tag</td>
          <td>${(_.sum(rows[rows.length - 1]["Pflegeheimbewohner*in"]) / daysSinceStart).toFixed()}</td>
        </tr>
        <tr>
          <td>Indikation nach Alter</td>
          <td>${rows[rows.length - 1]["Indikation nach Alter"]}</td>
        </tr>
        <tr>
          <td>Berufliche Indikation</td>
          <td>${rows[rows.length - 1]["Berufliche Indikation"]}</td>
        </tr>
        <tr>
          <td>Medizinische Indikation</td>
          <td>${rows[rows.length - 1]["Medizinische Indikation"]}</td>
        </tr>
        <tr>
          <td>Pflegeheimbewohner*in</td>
          <td>${rows[rows.length - 1]["Pflegeheimbewohner*in"]}</td>
        </tr>
      </tbody>
    </table>`;

  replace.sync({ files: 'dist/index_in_progress.html', from: `VACINATION_TABLE`, to: table });
}

function fillTemplate(kreis_id, canvasName, template){
  
  var onloadHtml = `
    var ctx = document.getElementById('${canvasName}Canvas').getContext('2d');
    window.canvas_age_incidence_${kreis_id} = new Chart(ctx, ${template});
    /* WINDOW_ONLOAD_NEEDLE */`;

  replace.sync({ files: 'dist/index_in_progress.html', from: `/* WINDOW_ONLOAD_NEEDLE */`, to: onloadHtml });
}

doVacintations();