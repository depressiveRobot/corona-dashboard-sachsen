const replace = require('replace-in-file');
const commons = require('./commons');
const moment = require('moment');
moment.locale("de_DE");
var loader = require('csv-load-sync');

var dates = [];
var values = {
  'retail_and_recreation_percent_change_from_baseline' : [],
  'grocery_and_pharmacy_percent_change_from_baseline' : [],
  'parks_percent_change_from_baseline' : [],
  'transit_stations_percent_change_from_baseline' :[],
  'workplaces_percent_change_from_baseline' :[],
  'residential_percent_change_from_baseline' : []
}

var csv = loader("dist/google-mobility-sn.csv");
csv.forEach(function(csvrow){
  if (csvrow.sub_region_1 == "Saxony" && moment().diff(csvrow.date, 'days') <= 50 ) {
    
    dates.push(`"${moment(csvrow['date']).format("DD.MM")}"`);
    values.retail_and_recreation_percent_change_from_baseline
      .push(csvrow['retail_and_recreation_percent_change_from_baseline']);

    values.grocery_and_pharmacy_percent_change_from_baseline
      .push(csvrow['grocery_and_pharmacy_percent_change_from_baseline']);

    values.parks_percent_change_from_baseline
      .push(csvrow['parks_percent_change_from_baseline']);

    values.transit_stations_percent_change_from_baseline
      .push(csvrow['transit_stations_percent_change_from_baseline']);

    values.workplaces_percent_change_from_baseline
      .push(csvrow['workplaces_percent_change_from_baseline']);

    values.residential_percent_change_from_baseline
      .push(csvrow['residential_percent_change_from_baseline']);
  }
})

replace.sync({ files: 'dist/index_in_progress.html', 
  from: "retail_and_recreation_percent_change_from_baseline", 
  to: values.retail_and_recreation_percent_change_from_baseline.toString() });
replace.sync({ files: 'dist/index_in_progress.html', 
  from: "grocery_and_pharmacy_percent_change_from_baseline", 
  to: values.grocery_and_pharmacy_percent_change_from_baseline.toString() });
replace.sync({ files: 'dist/index_in_progress.html', 
  from: "parks_percent_change_from_baseline", 
  to: values.parks_percent_change_from_baseline.toString() });
replace.sync({ files: 'dist/index_in_progress.html', 
  from: "transit_stations_percent_change_from_baseline", 
  to: values.transit_stations_percent_change_from_baseline.toString() });
replace.sync({ files: 'dist/index_in_progress.html', 
  from: "workplaces_percent_change_from_baseline", 
  to: values.workplaces_percent_change_from_baseline.toString() });
replace.sync({ files: 'dist/index_in_progress.html', 
  from: "residential_percent_change_from_baseline", 
  to: values.residential_percent_change_from_baseline.toString() });

replace.sync({ files: 'dist/index_in_progress.html', from: 'Einzelhandel_current', 
  to: values.retail_and_recreation_percent_change_from_baseline[values.retail_and_recreation_percent_change_from_baseline.length - 1] > 0 ? 
    "+" + values.retail_and_recreation_percent_change_from_baseline[values.retail_and_recreation_percent_change_from_baseline.length - 1] : 
    values.retail_and_recreation_percent_change_from_baseline[values.retail_and_recreation_percent_change_from_baseline.length - 1]});

replace.sync({ files: 'dist/index_in_progress.html', from: 'Lebensmittel_current', 
  to: values.grocery_and_pharmacy_percent_change_from_baseline[values.grocery_and_pharmacy_percent_change_from_baseline.length - 1] > 0 ? 
    "+" + values.grocery_and_pharmacy_percent_change_from_baseline[values.grocery_and_pharmacy_percent_change_from_baseline.length - 1] : 
    values.grocery_and_pharmacy_percent_change_from_baseline[values.grocery_and_pharmacy_percent_change_from_baseline.length - 1]});

replace.sync({ files: 'dist/index_in_progress.html', from: 'Parks_current', 
  to: values.parks_percent_change_from_baseline[values.parks_percent_change_from_baseline.length - 1] > 0 ? 
    "+" + values.parks_percent_change_from_baseline[values.parks_percent_change_from_baseline.length - 1] : 
    values.parks_percent_change_from_baseline[values.parks_percent_change_from_baseline.length - 1]});

replace.sync({ files: 'dist/index_in_progress.html', from: 'ÖPNV_current', 
  to: values.transit_stations_percent_change_from_baseline[values.transit_stations_percent_change_from_baseline.length - 1] > 0 ? 
    "+" + values.transit_stations_percent_change_from_baseline[values.transit_stations_percent_change_from_baseline.length - 1] : 
    values.transit_stations_percent_change_from_baseline[values.transit_stations_percent_change_from_baseline.length - 1]});

replace.sync({ files: 'dist/index_in_progress.html', from: 'Arbeitsplatz_current', 
  to: values.workplaces_percent_change_from_baseline[values.workplaces_percent_change_from_baseline.length - 1] > 0 ? 
    "+" + values.workplaces_percent_change_from_baseline[values.workplaces_percent_change_from_baseline.length - 1] :
    values.workplaces_percent_change_from_baseline[values.workplaces_percent_change_from_baseline.length - 1]});

replace.sync({ files: 'dist/index_in_progress.html', from: 'Wohnsitz_current', 
  to: values.residential_percent_change_from_baseline[values.residential_percent_change_from_baseline.length - 1] > 0 ? 
    "+" + values.residential_percent_change_from_baseline[values.residential_percent_change_from_baseline.length - 1] : 
    values.residential_percent_change_from_baseline[values.residential_percent_change_from_baseline.length - 1]});

replace.sync({ files: 'dist/index_in_progress.html', 
  from: "MOBILITY_LABELS", 
  to: dates.toString() });



