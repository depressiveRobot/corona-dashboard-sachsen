// https://www.arcgis.com/sharing/rest/content/items/f10774f1c63e40168479a1feb6c7ca74/data 
const replace = require('replace-in-file');
const fs = require('fs');
const _ = require('lodash');
const moment = require('moment');
var sql = require('../../scripts/sql.js');

async function run(){
  
  const currentCovidCases = sql.getCurrentCovidCases();
  const currentCovidCasesVentilated = sql.getCurrentCovidCasesVentilated();

  replace.sync({ files: 'dist/index_in_progress.html', from: `DIVI_LABELS`, 
    to: Object.keys(currentCovidCases).map(date => `"${moment(date).format("DD.MM.")}"`).join()});
  replace.sync({ files: 'dist/index_in_progress.html', from: `DIVI_CASES`, 
    to: Object.values(currentCovidCases).join() });
  replace.sync({ files: 'dist/index_in_progress.html', from: `DIVI_CASES_VENTILATED`, 
    to: Object.values(currentCovidCasesVentilated).join() });
}

run();