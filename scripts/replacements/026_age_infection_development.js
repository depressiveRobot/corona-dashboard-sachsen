// https://www.arcgis.com/sharing/rest/content/items/f10774f1c63e40168479a1feb6c7ca74/data 
const replace = require('replace-in-file');
const csv = require('../../scripts/csv.js');
const fs = require('fs');
const _ = require('lodash');
const moment = require('moment');
let path = require("path");
const commons = require('./commons');

async function fillTemplate(kreis_id){

  const rows = await csv.getRkiRows();

  const result = {
    'dates': [], 'A00-A04': [], 'A05-A14': [], 'A15-A34': [], 'A35-A59': [], 'A60-A79': [], 'A80+': []
  }
  
  commons.ageGroups.forEach(ageGroup => {
    result.dates = [];
    _.range(50).forEach(index => {
      result.dates.unshift(`"${moment().subtract(index + 1, 'days').format("D.M.")}"`)
      result[ageGroup].unshift(csv.getDistributionInAgeGroup(rows, ageGroup, "M|W", 14, 7, index))
    })
  })

  var infectionsTemplate = fs.readFileSync(path.join(__dirname, "../../html/age_incidence_template.js"), "utf8");
  infectionsTemplate = infectionsTemplate.replace(/KREIS_ID/g, kreis_id);
  infectionsTemplate = infectionsTemplate.replace(/GRAPH_TITLE/g, 'Neuinfektionen innerhalb von 7 Tagen nach Altersklassen');
  infectionsTemplate = infectionsTemplate.replace(/X_AXIS_LABEL/g, 'Datum');
  infectionsTemplate = infectionsTemplate.replace(/Y_AXIS_LABEL/g, 'Anzahl an Neuinfektionen');
  infectionsTemplate = infectionsTemplate.replace(/CONFIG_NAME/g, `ageInfectionDevelopment${kreis_id}Config`);
  infectionsTemplate = infectionsTemplate.replace(/AGE_INCIDENCE_DATES/g,        result['dates']);
  infectionsTemplate = infectionsTemplate.replace(/AGE_INCIDENCE_0_4_VALUES/g,   result['A00-A04']);
  infectionsTemplate = infectionsTemplate.replace(/AGE_INCIDENCE_5_14_VALUES/g,  result['A05-A14']);
  infectionsTemplate = infectionsTemplate.replace(/AGE_INCIDENCE_15_34_VALUES/g, result['A15-A34']);
  infectionsTemplate = infectionsTemplate.replace(/AGE_INCIDENCE_35_59_VALUES/g, result['A35-A59']);
  infectionsTemplate = infectionsTemplate.replace(/AGE_INCIDENCE_60_79_VALUES/g, result['A60-A79']);
  infectionsTemplate = infectionsTemplate.replace(/AGE_INCIDENCE_80_VALUES/g,    result['A80+']);
  infectionsTemplate += "\n/* JAVASCRIPT_NEEDLE */"

  var onloadHtml = `
    var ctx = document.getElementById('age-infection-development-canvas-${kreis_id}').getContext('2d');
    window.canvas_age_infection_developement_${kreis_id} = new Chart(ctx, ageInfectionDevelopment${kreis_id}Config);
    /* WINDOW_ONLOAD_NEEDLE */`;

  replace.sync({ files: 'dist/index_in_progress.html', from: `/* WINDOW_ONLOAD_NEEDLE */`, to: onloadHtml });
  replace.sync({ files: 'dist/index_in_progress.html', from: `/* JAVASCRIPT_NEEDLE */`, to: infectionsTemplate });
}

fillTemplate(14);