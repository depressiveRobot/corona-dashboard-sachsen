const replace = require('replace-in-file');
const moment = require('moment');
const commons = require('./commons');
moment.locale("de_DE");
var days = 21;
var loader = require('csv-load-sync');
var Client = require('pg-native');
var client = new Client();
client.connectSync();

commons.kreise.forEach(function(kreis_id){  

  var lis = ""
  var rows = client.querySync(`SELECT * FROM links WHERE kreis_id = ${kreis_id} and created > current_date - interval '7' day; ;`);
  rows.forEach(function(row){

    lis += `
      <li class="list-group-item">
        <a href="${row.link}">
          ${!row.source ? "" : row.source + ": " } ${row.text}
        </a>
      </li>
    `
  })

  var relevantLinks = "";
  if ( lis != "" ) {

    relevantLinks = `
      <div class="mt-3">
        <h5>Relevante Presseartikel und Links der letzten Woche</h5>
        <ul class="list-group-flush" style="padding-left: 0px;">
          ${lis}
        </ul>
      </div>
    `;
  }
  replace.sync({ files: 'dist/index_in_progress.html', from: `TABLE_${kreis_id}_RELEVANT_LINKS`, to: relevantLinks });
})
