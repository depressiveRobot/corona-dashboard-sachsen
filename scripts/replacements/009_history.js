const replace = require('replace-in-file');
const moment = require('moment');
moment.locale("de_DE");
var days = 21;
var loader = require('csv-load-sync');
var Client = require('pg-native');
var client = new Client();
client.connectSync();


var table = `
  <table class="table table-striped" id="history_table">
    <thead>
      <tr>
        <th scope="col">Bezeichnung</th>
        <th scope="col">Link zur Rechtsquelle</th>
        <th scope="col">Wesentlicher Inhalt</th>
        <th scope="col">erlassen am</th>
        <th scope="col">wirksam ab</th>
      </tr>
    </thead>
    <tbody>
  `;

var zeitlicherVerlaufEinschraenkungen = [
  {
    "Bezeichnung": "Erlass des SMS zum Umgang mit Großveranstaltungen",
    "Link zur Rechtsquelle": "https://www.sms.sachsen.de/download/Erlass_Grossveranstaltungen.pdf",
    "Wesentlicher Inhalt": "Verbot von Großveranstaltungen > 1.000 Personen",
    "erlassen am": "10.03.20",
    "wirksam ab": "12.03.20"
  },
  {
    "Bezeichnung": "Allgemeinverfügung zur Einstellung des Betriebs von Schulen und Kindertageseinrichtungen",
    "Link zur Rechtsquelle": "https://www.laenderrecht.de/media/upload//0205%20-%20SaechsABl_SDr_2020-04_LV.pdf",
    "Wesentlicher Inhalt": "Gestuftes Verfahren mit zweitägiger Übergangsfrist und anschließender Schließung mit Notbetreuung",
    "erlassen am": "13.03.20",
    "wirksam ab": "16.3. unterrichtsfrei 18.3. Schließung mit Notbetreuung"
  },
  {
    "Bezeichnung": "Gemeinsame Leitlinien zum einheitlichen Vorgehen zur weiteren Beschränkung von sozialen Kontakten im öffentlichen Bereich angesichts der Corona-Epidemie",
    "Link zur Rechtsquelle": "https://www.bundesregierung.de/breg-de/themen/meseberg/leitlinien-zum-kampf-gegen-die-corona-epidemie-1730942",
    "Wesentlicher Inhalt": "Schließung Bars, Clubs, Theater, Oper, Museen, Sportanlagen u.Ä.  Schließung aller Läden außer der Grundversorgung  Verbot aller Zusammenkünfte (Sport, Religionsgemeinschaften, Vereine etc.) >50 Personen  Restaurants nur 6-18 Uhr",
    "erlassen am": "16.03.20",
    "wirksam ab": "durch Umsetzung in den Ländern"
  },
  {
    "Bezeichnung": "Allgemeinverfügung zu Komplettverbot aller Veranstaltungen, Schließung von Läden, Kinos etc.",
    "Link zur Rechtsquelle": "https://www.sms.sachsen.de/download/SMS-Allgemeinverfuegung-Corona-Veranstaltungen-bf.pdf",
    "Wesentlicher Inhalt": "Landesrechtliche Umsetzung der gemeinsamen Leitlinien",
    "erlassen am": "18.03.20",
    "wirksam ab": "19.03.20"
  },
  {
    "Bezeichnung": "Allgemeinverfügung zu Veranstaltungen (Verschärfung)",
    "Link zur Rechtsquelle": "https://www.laenderrecht.de/media/upload//0207%20-%20SaechsABl_SDr_2020-05_LV.pdf",
    "Wesentlicher Inhalt": "Komplettschließung Restaurants",
    "erlassen am": "20.03.20",
    "wirksam ab": "22.03.20"
  },
  {
    "Bezeichnung": "Allgemeinverfügung des Innen- und Sozialministeriums in Abstimmung mit den kommunalen Landesverbänden",
    "Link zur Rechtsquelle": "https://www.coronavirus.sachsen.de/download/AllgV-Corona-Ausgangsbeschraenkungen_22032020.pdf",
    "Wesentlicher Inhalt": "Verlassen der Wohnung nur aus triftigem Grund  Verbot von Zusammenkünften aller Art (geringfügige Ausnahmen z.B. Beerdigungen)",
    "erlassen am": "22.03.20",
    "wirksam ab": "23.03.20"
  },
  {
    "Bezeichnung": "Verordnung des Sächsischen Staatsministeriums für Soziales und Gesellschaftlichen Zusammenhalt zum Schutz vor dem Coronavirus SARS-CoV-2 und COVID-19",
    "Link zur Rechtsquelle": "https://www.coronavirus.sachsen.de/download/Fassung-RV-SaechsCoronaSchVO_31032020.pdf",
    "Wesentlicher Inhalt": "wie Allgemeinverfügung (geringfügige Änderungen bei Wochenmärkten, Aktivitäten auch zu zweit für Alleinlebende)",
    "erlassen am": "31.03.20",
    "wirksam ab": "01.04.20"
  },
  {
    "Bezeichnung": "Allgemeinverfügung zur Einstellung des Betriebs von Schulen und Kindertageseinrichtungen",
    "Link zur Rechtsquelle": "",
    "Wesentlicher Inhalt": "Erweiterung der Notbetreuung in Kitas und Schulen um weitere Berufsgruppen  Ermöglichung von Prüfungen und Konsultationen für Abschlussjahrgänge",
    "erlassen am": "17.04.20",
    "wirksam ab": "20.04.20"
  },
  {
    "Bezeichnung": "Verordnung des Sächsischen Staatsministeriums für Soziales und Gesellschaftlichen Zusammenhalt zum Schutz vor dem Coronavirus SARS-CoV-2 und COVID-19 (Neufassung)",
    "Link zur Rechtsquelle": "https://www.revosax.sachsen.de/vorschrift/18661",
    "Wesentlicher Inhalt": "Öffnung kleinerer Geschäfte bis 800 qm  Wegfall der  Ausgangsbeschränkungen  Einführung der Pflicht zum Tragen von Mund-Nasen-Schutz",
    "erlassen am": "17.04.20",
    "wirksam ab": "20.04.20"
  }
]

zeitlicherVerlaufEinschraenkungen.reverse().forEach(function(row){
  table += `
    <tr>
      <td>${row['Bezeichnung']}</td>
      <td><a target="_blank" href="${row['Link zur Rechtsquelle']}">Link</a></td>
      <td>${row['Wesentlicher Inhalt']}</td>
      <td>${row['erlassen am']}</td>
      <td>${row['wirksam ab']}</td>
    </tr>
  `
})
table += `</tbody></table>`;

replace.sync({ files: 'dist/index_in_progress.html', from: `TABLE_HISTORY`, to: table });
