const replace = require('replace-in-file');
const moment = require('moment');
const commons = require('./commons');
const sql = require('../../scripts/sql.js');

const template = `
    <div class="row mt-3">
      <div class="col-md-12">
        <h2 id="KREIS_SLUG">KREIS_NAME</h2>
        <p>Alle weiteren Informationen zum Infektionsgeschehen finden sie auf der <a target="_blank" href="GESUNDHEITSAMT_URL">Seite des Gesundheitsamts</a></p>
      </div>
      <div class="col-md-12" id="KREIS_ID-most-important-info">
        KREIS_ID_MOST_IMPORTANT_INFO
      </div>
      <div class="col-md-12">
        <h3 data-toc-text="Neuinfektionen">Neuinfektionen</h3>
        <canvas id="diff-yesterday-canvas-KREIS_ID"></canvas>
      </div>
      <div class="col-md-12 mt-4" style="display: none;" id="gemeindeChartKREIS_IDColumn">
        <h3>Gemeinden</h3>
        <canvas id="gemeindeChartKREIS_IDCanvas"></canvas>
        <div class="alert alert-info" role="alert">
          Der Graph hat zwei Achsen. Links kann man die Neuinfektionen der letzten 7 Tage und rechts die 7-Tages Inzidenz ablesen.
        </div>
      </div>
      <div class="col-md-12 mt-4 mb-4" id="kreis-id-KREIS_ID-gemeinde-table">
        TABLE_KREIS_ID_GEMEINDEN_TABLE
      </div>
      <div class="col-md-12 mt-4">
        <h3 data-toc-text="Wachstumsrate">Wachstumsraten der 7-Tages-Fallzahlen</h3>
        <p>Darstellung auf Basis der Berechnungen von <a target="_blank" href="https://twitter.com/f2135/">Prof. Dr. Christian Fries</a> der 
            <a target="_blank" href="https://www.fm.mathematik.uni-muenchen.de/personen/professors/fries/index.html">LMU München</a>. Vielen Dank! ♥️
        </p> 
        <canvas id="growth-canvas-KREIS_ID"></canvas>
      </div>
      <div class="col-md-INFECTION_COLUMN_WIDTH" id="infectionDateDistributionKREIS_IDCanvasDiv">
        <canvas id="infectionDateDistributionKREIS_IDCanvas"></canvas>
        <p>
          <small>
            Die Daten vom <a href="https://experience.arcgis.com/experience/478220a4c454480e823b17327b2bf1d4">RKI</a> enthalten einen Zeitverzug durch den Meldeverzug der Gesundheitsämter. Werte ändern sich daher auch für vergangene Tage immer wieder.
            <b>Letzte Aktualisierung: ${moment(sql.getMaxDate()).format("DD.MM.YYYY")} 00:00 Uhr</b>
          </small>
        </p>
      </div>
      <div class="col-md-12 mt-3">
        <h3>Wöchentliche Auswertungen</h3>
      </div>
      <div class="col-md-12">
        <canvas id="infectedWeekValuesKREIS_IDCanvas"></canvas>
      </div>
      <div class="col-md-12">
        <canvas id="deathWeekValuesKREIS_IDCanvas"></canvas>
      </div>
      <div class="col-md-12 mt-3">
        <h3 data-toc-text="Krankenhaus (Intensivstation)">Übersicht zu Krankenhausbelegungen</h3>
      </div>
      <div class="col-md-12">
        <canvas id="diviCovidCasesKREIS_IDCanvas"></canvas>
      </div>
      <div class="col-md-12">
        <canvas id="diviITSBedsKREIS_IDCanvas"></canvas>
      </div>
      <div class="col-md-12 mt-3">
        <h3 data-toc-text="Alter und Geschlecht" class="text-center">Verteilung nach Alter und Geschlecht</h3>
      </div>
      <div class="col-md-12">
        <canvas id="ageGenderKREIS_IDCanvas"></canvas>
      </div>
      <div class="col-md-12">
        <canvas id="ageGenderLastSevenDaysKREIS_IDCanvas"></canvas>
      </div>
      <div class="col-md-12">
        <canvas id="age-incidence-canvas-KREIS_ID"></canvas>
        TABLE_KREIS_ID_RELEVANT_LINKS
      </div>
    </div>
  `
var allTemplates = ""; 
commons.kreise.filter(x => x != 14).forEach(function(kreis_id){  

  var html = template.replace("KREIS_NAME", commons.names[kreis_id]);
  html = html.replace(/INFECTION_COLUMN_WIDTH/g, kreis_id < 4 ? "7" : "5");
  html = html.replace(/KREIS_ID/g, kreis_id);
  html = html.replace(/KREIS_SLUG/g, commons.slugs[kreis_id]);
  html = html.replace("GESUNDHEITSAMT_URL", sql.getHealthOfficeUrl(kreis_id));

  if ( kreis_id > 3 ) {
    html = html.replace("GEMEINDE_CHART", `
    <div class="col-md-12" style="display: none;">
      <canvas id="gemeindeChart${kreis_id}Canvas"></canvas>
    </div>
    `)
  }
  
  allTemplates += html;
});

replace.sync({ files: 'dist/index_in_progress.html', from: `KREIS_TEMPLATES`, to: allTemplates});