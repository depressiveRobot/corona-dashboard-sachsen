const replace = require('replace-in-file');
var fs = require('fs');
const moment = require('moment');
var Client = require('pg-native')
var client = new Client()
client.connectSync()
var days = 21;

var results = {
    'labels'    : [],
    'death'     : [],
    'recovered' : [],
    'active'    : [],
    'infected'  : []
  }

var rows = client.querySync("SELECT date, value, type FROM measurement WHERE kreis_id = 14 and source = 'rki' order by date;");
rows.forEach(function(measurement){

  if (!results.labels.includes(`"${moment(measurement.date).format("DD.MM")}"`))
    results.labels.push(`"${moment(measurement.date).format("DD.MM")}"`);

  if (measurement.type == 'death') {
    results.death.push(measurement.value)
  }
  else if (measurement.type == 'recovered') {
    results.recovered.push(measurement.value)
  }
  else if (measurement.type == 'infected') {
    results.infected.push(measurement.value)

    var infected12Days = client.querySync(
      `SELECT value
       FROM measurement 
       WHERE kreis_id = ${14} and date::date = '${moment(measurement.date, "DD. MMMM YYYY, HH:mm z").format()}'::date - integer '12' and type = 'infected';
    `)[0];
    var infected14Days = client.querySync(
      `SELECT value
       FROM measurement 
       WHERE kreis_id = ${14} and date::date = '${moment(measurement.date, "DD. MMMM YYYY, HH:mm z").format()}'::date - integer '14' and type = 'infected';
    `)[0];
    var infected16Days = client.querySync(
      `SELECT value
       FROM measurement 
       WHERE kreis_id = ${14} and date::date = '${moment(measurement.date, "DD. MMMM YYYY, HH:mm z").format()}'::date - integer '16' and type = 'infected';
    `)[0];

    var infected  = results.infected[results.infected.length - 1]
    var recovered = ((0.1 * (infected12Days ? infected12Days.value : 0)) + 
                     (0.8 * (infected14Days ? infected14Days.value : 0)) + 
                     (0.1 * (infected16Days ? infected16Days.value : 0)))
    var dead      = results.death[results.death.length - 1]
    
    var active = infected - Number(recovered).toFixed(0) - dead;
    results.active.push(isNaN(active) ? null : active);
  }
  else {
    // throw 'unknown state of measurement'
  }
})

while (results.death.length < results.labels.length ) { results.death.unshift(null); }
while (results.recovered.length < results.labels.length ) { results.recovered.unshift(null); }
while (results.active.length < results.labels.length ) { results.active.unshift(null); }
while (results.infected.length < results.labels.length ) { results.infected.unshift(null); }

results.labels = results.labels.slice(results.labels.length - (1 + days), results.labels.length);
results.death = results.death.slice(results.death.length - (1 + days), results.death.length);
results.recovered = results.recovered.slice(results.recovered.length - (1 + days), results.recovered.length);
results.active = results.active.slice(results.active.length - (1 + days), results.active.length);
results.infected = results.infected.slice(results.infected.length - (1 + days), results.infected.length);

results.activeCases = []
for (var i = 0; i <= results.recovered.length; i++) {
  results.activeCases.push(results.infected[i] - results.recovered[i] - results.death[i]);
}

replace.sync({ files: 'dist/index_in_progress.html', from: 'INFECTED_SAXONY_VALUES', to: results.infected.toString() });
replace.sync({ files: 'dist/index_in_progress.html', from: 'INFECTED_SAXONY_LABELS', to: results.labels.toString() });
replace.sync({ files: 'dist/index_in_progress.html', from: 'DEATH_SAXONY_VALUES',    to: results.death.toString() });
replace.sync({ files: 'dist/index_in_progress.html', from: 'RECOVERD_SAXONY_VALUES', to: results.recovered.toString() });
replace.sync({ files: 'dist/index_in_progress.html', from: 'ACTIVE_SAXONY_VALUES',   to: results.active.toString() });
replace.sync({ files: 'dist/index_in_progress.html', from: 'ACTIVE_CASES_SAXONY_VALUES',   to: results.activeCases.toString() });