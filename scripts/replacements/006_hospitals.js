const replace = require('replace-in-file');
const moment = require('moment');
moment.locale("de_DE");
const sql = require('../../scripts/sql.js');

var tranlations = {
  'meldebereichAnz': "meldebereichAnz",
  'faelleCovidAktuell': "In Behandlung",
  'faelleCovidAktuellBeatmet': "Invasiv beatmet",
  'faelleCovidAktuellBeatmetToCovidAktuellPercent': "Invasiv beatmet %",
  'intensivBettenBelegt': "ITS belegt",
  'intensivBettenFrei': "ITS frei",
  'intensivBettenGesamt': "ITS Maximum",
  'covidToIntensivBettenPercent': "Anteil COVID an ITS Max.",
  'intensivBettenNotfall7d': "Notfallreserve",
}

var rows = "";
for (const [key, value] of Object.entries(tranlations)) {
  if (key != 'meldebereichAnz') {
    rows += 
      `<tr>
        <td>${value}:</td>
        <td>${sql.getDiviValue(key, sql.getMaxDate(key, 'DIVI'))}</td>
        <td>${sql.getDiviValue(key, moment(sql.getMaxDate(key, 'DIVI')).subtract(1, 'days')) || '-'}</td>
      </tr>`
  }
}

var table = `
  <table class="table">
    <thead>
      <tr>
        <th scope="col">DIVI Intensivregister</th>
        <th scope="col">Aktuell</th>
        <th scope="col">Gestern</th>
      </tr>
    </thead>
    <tbody>
      ${rows}
    </tbody>
  </table>
  <p><small><a href="https://www.intensivregister.de/#/intensivregister">DIVI Intensivregister</a>: 
  Letzte Aktualisierung am: ${moment(sql.getMaxDate('faelleCovidAktuell', 'DIVI')).format("DD.MM.YYYY")} Uhr</small></p>
`
replace.sync({ files: 'dist/index_in_progress.html', from: `TABLE_DIVI`, to: table });
