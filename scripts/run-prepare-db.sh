#!/bin/bash

POSTGRES="PGUSER=${PGUSER:=corona} PGHOST=${PGHOST:=localhost} PGPASSWORD=${PGPASSWORD} PGDATABASE=${PGDATABASE:=corona} PGPORT=${PGPORT:=5432}"

env $POSTGRES psql --quiet -c "DROP TABLE IF EXISTS measurement"
env $POSTGRES psql --quiet -c "DROP TABLE IF EXISTS links"
env $POSTGRES psql --quiet -c "DROP TABLE IF EXISTS kreise_sachsen"
env $POSTGRES psql --quiet -f sql/tables.sql
env $POSTGRES psql --quiet -f sql/inserts.sql