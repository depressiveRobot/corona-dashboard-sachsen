// https://www.arcgis.com/sharing/rest/content/items/f10774f1c63e40168479a1feb6c7ca74/data 
const parse = require('csv-parse/lib/sync');
const fs = require('fs');
const _ = require('lodash');
const moment = require('moment');
const fastcsv = require('fast-csv');

const landkreise = {
  "14511": "SK Chemnitz",
  "14521": "LK Erzgebirgskreis",
  "14522": "LK Mittelsachsen",
  "14523": "LK Vogtlandkreis",
  "14524": "LK Zwickau",
  "14612": "SK Dresden",
  "14625": "LK Bautzen",
  "14626": "LK Görlitz",
  "14627": "LK Meißen",
  "14628": "LK Sächsische Schweiz-Osterzgebirge",
  "14713": "SK Leipzig",
  "14729": "LK Leipzig",
  "14730": "LK Nordsachsen"
}

async function preprocess(){
  
  const content = await fs.promises.readFile(`dist/risklayer_COVID19.csv`)
  const risklayer = parse(content, {columns : true});
  const ags = Object.keys(landkreise)
  const lines = [];
  risklayer.forEach(function(row){
    const line = [row.time_iso8601.replace(/T.*/, "")]
    ags.forEach(ags => line.push(row[ags]));
    lines.push(line);
  })
  lines.pop(); // removes last duplicate line 

  const newLines = [];
  for (let last = lines.length - 1; last >= 1;) {
    for (let previous = lines.length - 2; previous >= 0; previous--, last--) {

      const lastLine = lines[last];
      const prevLine = lines[previous];
      
      const line = [];
      for (let i = 0; i < lastLine.length; i++) {
        if (i==0)
          line.push(lastLine[0])
        else {
          line.push(lastLine[i] - prevLine[i] < 0 ? 0 : lastLine[i] - prevLine[i])
        }
      }
      newLines.push(line);
    }
  }
  newLines.push(["date", ...ags])

  fastcsv
      .write(newLines.reverse(), { headers: true })
      .pipe(fs.createWriteStream(`dist/AnzahlFall-risklayer-by-ags.csv`));
}

preprocess();