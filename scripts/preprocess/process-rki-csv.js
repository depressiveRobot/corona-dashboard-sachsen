// https://www.arcgis.com/sharing/rest/content/items/f10774f1c63e40168479a1feb6c7ca74/data 
const parse = require('csv-parse/lib/sync');
const fs = require('fs');
const _ = require('lodash');
const moment = require('moment');
const fastcsv = require('fast-csv');

const landkreise = {
  "14511": "SK Chemnitz",
  "14521": "LK Erzgebirgskreis",
  "14522": "LK Mittelsachsen",
  "14523": "LK Vogtlandkreis",
  "14524": "LK Zwickau",
  "14612": "SK Dresden",
  "14625": "LK Bautzen",
  "14626": "LK Görlitz",
  "14627": "LK Meißen",
  "14628": "LK Sächsische Schweiz-Osterzgebirge",
  "14713": "SK Leipzig",
  "14729": "LK Leipzig",
  "14730": "LK Nordsachsen"
}

async function preprocess(){
  
  const content = await fs.promises.readFile(`dist/RKI_COVID19.csv`)
  const sachsen = parse(content, {columns : true}).filter(item => item.IdBundesland == '14');
  const fromDate = moment("2021-01-01");
  const toDate   = moment();

  ["AnzahlFall", "AnzahlGenesen", "AnzahlTodesfall"].forEach(state => {

    const lines = [["date", ...Object.keys(landkreise)]];
    for (var currentDate = moment(fromDate); currentDate.diff(toDate, 'days') < 0; currentDate.add(1, 'days')) {

      var line = [currentDate.format("YYYY-MM-DD")];
      for (const [key, value] of Object.entries(landkreise)) {
        line.push(sachsen
          .filter(item => (item.NeuerFall == '1' || item.NeuerFall == '0') &&
                        item.IdLandkreis == key && 
                        moment(currentDate).isSame(moment(item.Meldedatum.replace(" 00:00:00", ""), "YYYY/MM/DD"), 'day'))
          .reduce((accumulator, landkreisValue) => accumulator + parseInt(landkreisValue[state]), 0))
      }
      lines.push(line);
    }  
    fastcsv
      .write(lines, { headers: true })
      .pipe(fs.createWriteStream(`dist/${state}-rki-by-ags.csv`));
  })  
}

async function statisticalOverview(){
  
  // Read the content
  const content = await fs.promises.readFile(`dist/RKI_COVID19.csv`)
  // Parse the CSV content
  const records = parse(content, {columns : true})
  
  const sachsen = records.filter(item => item.IdBundesland == '14');
  console.log(`Sachsen hat bisher ${sachsen.length} Einträge.`)

  console.log(`Sachsen hat folgende Verteilung bei den betroffenen "Altersgruppe"`)
  console.log(_.countBy(sachsen.map(record => record.Altersgruppe)))
  console.log(`Sachsen hat folgende Verteilung bei den betroffenen "Altersgruppe2"`)
  console.log(_.countBy(sachsen.map(record => record.Altersgruppe2)))
  console.log(`Sachsen hat folgende Verteilung bei den betroffenen "Geschlecht"`)
  console.log(_.countBy(sachsen.map(record => record.Geschlecht)))
  console.log(`Sachsen hat folgende Verteilung bei den betroffenen "NeuerFall"`)
  console.log(_.countBy(sachsen.map(record => record.NeuerFall)))
  console.log(`Sachsen hat folgende Verteilung bei den betroffenen "NeuerTodesfall"`)
  console.log(_.countBy(sachsen.map(record => record.NeuerTodesfall)))
  console.log(`Sachsen hat folgende Verteilung bei den betroffenen "NeuGenesen"`)
  console.log(_.countBy(sachsen.map(record => record.NeuGenesen)))
  console.log(`Sachsen hat folgende Verteilung bei den betroffenen "Landkreis"`)
  console.log(_.countBy(sachsen.map(record => record.Landkreis)))
  console.log(`Sachsen hat folgende Verteilung bei den betroffenen "IstErkrankungsbeginn"`)
  console.log(_.countBy(sachsen.map(record => record.IstErkrankungsbeginn)))

  const male = sachsen.filter(item => item.Geschlecht == "M");
  console.log(`Sachsen hat bisher ${male.length} männliche Einträge.`)

  const female = sachsen.filter(item => item.Geschlecht == "W");
  console.log(`Sachsen hat bisher ${female.length} weibliche Einträge.`)

  const unknown = sachsen.filter(item => item.Geschlecht == "unbekannt");
  console.log(`Sachsen hat bisher ${unknown.length} Einträge ohne angegebenes Geschlecht.`)
  
  console.log(sachsen[1])

  for (const [key, value] of Object.entries(landkreise)) {
    
    const landkreisValues = sachsen.filter(item => item.IdLandkreis == key);
    const faelleGesamt = landkreisValues.filter(item => item.NeuerFall == '1' || item.NeuerFall == '0')
      .reduce((accumulator, landkreisValue) => accumulator + parseInt(landkreisValue.AnzahlFall), 0)
    console.log(`${value} hat bisher ${faelleGesamt} Fälle ingesamt.`);

    ['M', 'W'].forEach(gender => {
      ['A00-A04', 'A05-A14', 'A15-A34', 'A35-A59', 'A60-A79', 'A80+'].forEach(years => {

        const x = landkreisValues.filter(item => 
          (item.NeuerFall == '1' || item.NeuerFall == '0') && 
          item.Geschlecht == gender && 
          item.Altersgruppe == years )
          .reduce((accumulator, landkreisValue) => accumulator + parseInt(landkreisValue.AnzahlFall), 0)
    
        console.log(`${gender} - ${years}: ${x}`)
      })
      console.log(``);
    })
  }
}

preprocess();
// statisticalOverview();