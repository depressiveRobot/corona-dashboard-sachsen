const commander = require('commander'); 
const moment = require('moment');
const _ = require('lodash'); 
const commons = require('./replacements/commons');
const program = new commander.Command();
const sql = require('./sql');
const helper = require('./helper');

program
  .option('-d, --debug', 'output extra debugging')
  .option('-f, --from-date', 'starting date')
  .option('-t, --to-date', 'end date');

const options = program.opts();

program
  .command('incidence <district> [days] [dateFormat]')
  .description('Calculate 7-day incidence for given district')
  .action((district, days, dateFormat) => {
    
    const district_id   = Number.isFinite(_.toNumber(district)) ? district : _.invertBy(commons.names)[district];
    const district_name = commons.names[district_id];
    
    if (district_id <= 14 && district_id >= 0) {

      if (days) {
        const results = {};
        for (let index = 0; index < days; index++) {
          results[moment().subtract(index + 1, 'days').format(dateFormat || "YYYY-MM-DD")] = Number(sql.getXDaysIncidenceForDistrict(district_id, 7 + index, 1, index));
        }
        console.log(results)
      }
      else {
        console.log(`7-day incidence for ${district_name} (id: ${district_id}) is ${sql.getXDaysIncidenceForDistrict(district_id, 7)}`);
      }
    }
    else
      console.log(`District ${district} unknown.`)
  });

  program
  .command('incidence-diff <district> [days] [dateFormat]')
  .description('Calculate 7-day incidence diff to previous day for given district')
  .action((district, days, dateFormat) => {
    
    const district_id   = Number.isFinite(_.toNumber(district)) ? district : _.invertBy(commons.names)[district];
    const district_name = helper.getName(district_id);
    const results = {};

    for (let index = 0; index < days; index++) {
      
      const yesterdayIncidence = sql.getXDaysIncidenceForDistrict(district_id, 7 + index + 1, 2, 1 + index);
      const todayIncidence     = sql.getXDaysIncidenceForDistrict(district_id, 7 + index,     2, 0 + index);
      
      results[moment().subtract(index + 1, 'days').format(dateFormat || "YYYY-MM-DD")] = todayIncidence - yesterdayIncidence;
    }

    console.log(results)


    
    // if (district_id <= 14 && district_id >= 0) {

    //   if (days) {
    //     const results = {};
    //     for (let index = 0; index < days; index++) {
    //       results[moment().subtract(index + 1, 'days').format(dateFormat || "YYYY-MM-DD")] = Number(sql.getXDaysIncidenceForDistrict(district_id, 7 + index, 1, index));
    //     }
    //     console.log(results)
    //   }
    //   else {
    //     console.log(`7-day incidence for ${district_name} (id: ${district_id}) is ${sql.getXDaysIncidenceForDistrict(district_id, 7)}`);
    //   }
    // }
    // else
    //   console.log(`District ${district} unknown.`)
  });

program.parse(process.argv);