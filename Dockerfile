# FROM node:13
FROM registry.gitlab.com/gerbsen/corona-dashboard-sachsen-docker-image

# Create app directory
WORKDIR /dashboard

# Bundle app source
COPY data /dashboard/data
COPY html /dashboard/html
COPY scripts /dashboard/scripts
COPY sql /dashboard/sql
RUN mkdir /dashboard/dist

CMD [ "./scripts/run-all.sh" ]