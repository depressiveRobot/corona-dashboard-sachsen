#!/usr/bin/env Rscript
# Autor: Initiative Grundrechte, https://twitter.com/IGrundrechte

library(EpiEstim)
library(optparse)
# install.packages('optparse', repos = 'http://cran.us.r-project.org')

option_list = list(
  make_option(c("-f", "--file"), type="character", default=NULL, 
              help="dataset file name", metavar="character"),
  make_option(c("-o", "--out"), type="character", default=NULL, 
              help="output file name [default= %default]", metavar="character")
); 

parser = parse_args(OptionParser(option_list=option_list))

if(!is.null(parser$file)) {
    if(!is.null(parser$out)) {
      # read the file
      data <- read.csv(parser$file, header=T)
      # parse the dates to acutal dates
      data$dates <- as.Date(data$dates, format="%Y-%m-%d")

      # Verarbeiten der Daten in EpiEstim
      # Verwendung eines Seriellen Intervalls mit 
      # Mittelwert 5,0 und Standardabweichung 1,9 (Ferretti u.a. 2020),
      # analog zu: https://www.genstat.imise.uni-leipzig.de/sites/www.genstat.imise.uni-leipzig.de/files/files/uploads/Bulletin_covid19_sachsen_leipzigPlus_2020_04_11_v5.pdf
      res_parametric_si <- estimate_R(data, method="parametric_si", 
        config = make_config(list(mean_si = 5.0, std_si = 1.9)))

      write.csv(res_parametric_si$R, file = parser$out)
    } else {
       print("--out parameter must be provided. See script usage (--help)")
    }
} else {
   print("--file parameter must be provided. See script usage (--help)")
}
